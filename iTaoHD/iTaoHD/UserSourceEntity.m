//
//  UserSourceEntity.m
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "UserSourceEntity.h"


@implementation UserSourceEntity

@dynamic desc;
@dynamic icon;
@dynamic isCustom;
@dynamic name;
@dynamic sid;
@dynamic sortid;
@dynamic themeid;
@dynamic itemtype;
@end
