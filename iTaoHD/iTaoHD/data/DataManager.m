//
//  DataManager.m
//  wine
//
//  Created by LiZujian on 13-6-23.
//  Copyright (c) 2013年 LiZujian. All rights reserved.
//

#import "DataManager.h"
//#import "JSONKit.h"
//#import "WineDataBaseModel.h"
//#import "Members.h"
#import "UtilManager.h"
#import "CuzyAdSDK.h"
#import "AuthorizeManager.h"
#import "CommentRequest.h"

@interface DataManager ()<CuzyAdSDKDelegate>
@property (nonatomic, assign) PicSizeType psTyPE;
@property (nonatomic, retain) NSString *sort;
@end

@implementation DataManager
@synthesize delegate = _delegate;
@synthesize isIphone5 = _isIphone5;
DEF_SINGLETON(DataManager)

-(void)dealloc
{
    _delegate = nil;
    [super dealloc];
}

-(id)init{
    self = [super init];
    if (self) {
        if(UIScreenHeight > 480)
            _isIphone5 = YES;
        else
            _isIphone5 = NO;
    }
    return self;
}

- (void)requstTBKItemsWithKeyWord:(NSString*)searchKey
                     WithThemeID:(NSString *)themeId
                 WithPicSizeType:(PicSizeType)PSType
                        WithSort:(NSString *)sort
                   WithPageIndex:(NSInteger) pageIndex
                    withDelegate:(id<DataManagerDelegate>)aDelegate{
    _psTyPE = PSType;
    self.sort = sort;
    [self requstTBKItemsWithKeyWord:searchKey WithThemeID:themeId WithPageIndex:pageIndex withDelegate:aDelegate];
    
}

-(void)requstTBKItemsWithKeyWord:(NSString*)searchKey WithThemeID:(NSString *)themeId WithPicSizeType:(PicSizeType)PSType WithPageIndex:(NSInteger) pageIndex withDelegate:(id<DataManagerDelegate>)aDelegate{
    _psTyPE = PSType;
    [self requstTBKItemsWithKeyWord:searchKey WithThemeID:themeId WithPageIndex:pageIndex withDelegate:aDelegate];
    
}
-(void)requstTBKItemsWithKeyWord:(NSString *)searchKey WithThemeID:(NSString *)themeId WithPageIndex:(NSInteger)pageIndex withDelegate:(id<DataManagerDelegate>)aDelegate
{
    _delegate = aDelegate;
    if (![AuthorizeManager sharedInstance].isAuthorized) {
        [[AuthorizeManager sharedInstance] requestForAuthorizeSucc:^{
            [self startThreadWork_fetchTBKItems:searchKey WithThemeID:themeId WithPageIndex:pageIndex];
        }
        Fail:^{
            [self dataIsReadyWithArr:nil];
        }];
        return;
    }
    [self startThreadWork_fetchTBKItems:searchKey WithThemeID:themeId WithPageIndex:pageIndex];
}

- (void)requestTBKShopItemsWithKeyword:(NSString*)searchKey WithPageIndex:(NSInteger) pageIndex withDelegate:(id<DataManagerDelegate>)aDelegate{
    _delegate = aDelegate;
    if (![AuthorizeManager sharedInstance].isAuthorized) {
        [[AuthorizeManager sharedInstance] requestForAuthorizeSucc:^{
            [self startThreadWork_fetchTBKItems:searchKey WithPageIndex:pageIndex];
        }
                                                              Fail:^{
                                                                  [self dataIsReadyWithArr:nil];
                                                              }];
        return;
    }
    [self startThreadWork_fetchTBKItems:searchKey WithPageIndex:pageIndex];
}
-(void)dataIsReadyWithArr:(NSMutableArray *)dataArr{
    if (dataArr && dataArr.count > 0)
    {
        if (_delegate && [_delegate respondsToSelector:@selector(updateViewForSuccess:)])
        {
            [_delegate updateViewForSuccess:dataArr];
        }
    
        if ([[dataArr lastObject] isKindOfClass:[CuzyTBKItem class]]) {
            CommentRequest *reqComment = [[CommentRequest alloc] init];
            [reqComment synchronizeItem:dataArr withDelegate:nil];
        }
    }
    else
    {
        if (_delegate && [_delegate respondsToSelector:@selector(updateViewForError:)])
        {
            [_delegate updateViewForError:nil];
        }
    }
    
}

-(void)startThreadWork_fetchTBKItems:(NSString*)searchKey WithThemeID:(NSString *)themeId WithPageIndex:(NSInteger) pageIndex{
    
    
    dispatch_async(kBgQueue, ^{
       
        CuzyAdSDK *CASDK = [CuzyAdSDK sharedAdSDK];
        [CASDK setRawItemPageNumber:@"40"];
        if (self.sort) {
            [CASDK setItemSortingMethod:self.sort];
        }else{
            [CASDK setItemSortingMethod:@"commission_rate_desc"];
        }
        [CASDK setRawItemPicSize:@"600x600"];
        NSArray *array = nil;
        if (themeId || searchKey) {
            array = [CASDK fetchRawItemArraysWithThemeID:themeId
                                        orSearchKeywords:searchKey
                                           withPageIndex:pageIndex];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableArray* returnArray = nil;
            if (array) {
                returnArray = [[NSMutableArray alloc] initWithArray:array];
            }
            //update the UI in main queue;
            [self dataIsReadyWithArr:returnArray];
        });
        
    });
    return;
    dispatch_queue_t downloadQueue = dispatch_queue_create("ItemQueue", NULL);
    dispatch_async(downloadQueue, ^{

        CuzyAdSDK *CASDK = [CuzyAdSDK sharedAdSDK];
        [CASDK setRawItemPageNumber:@"20"];
        if (self.sort) {
            [CASDK setItemSortingMethod:self.sort];
        }else{
            [CASDK setItemSortingMethod:@"commission_rate_desc"];
        }
        [CASDK setRawItemPicSize:@"600x600"];
        NSArray *array = nil;
        if (themeId || searchKey) {
            array = [CASDK fetchRawItemArraysWithThemeID:themeId
                                        orSearchKeywords:searchKey
                                           withPageIndex:pageIndex];
        }else{
            [self dataIsReadyWithArr:nil];
            return;
        }
        NSMutableArray* returnArray = [[NSMutableArray alloc] initWithArray:array];
        dispatch_async(dispatch_get_main_queue(), ^{
            //update the UI in main queue;
            [self dataIsReadyWithArr:returnArray];
        });
    });
    dispatch_release(downloadQueue); //won’t actually go away until queue is empty
}

-(void)startThreadWork_fetchTBKItems:(NSString*)searchKey WithPageIndex:(NSInteger) pageIndex{
    dispatch_queue_t downloadQueue = dispatch_queue_create("ItemQueue", NULL);
    dispatch_async(downloadQueue, ^{
        
        CuzyAdSDK *CASDK = [CuzyAdSDK sharedAdSDK];
        [CASDK setRawItemPageNumber:@"40"];
        [CASDK setRawItemPicSize:@"600x600"];
        NSArray *array = nil;
        if (searchKey) {
            array = [[CuzyAdSDK sharedAdSDK] fetchTBshopItemsWithKeyword:searchKey WithPageIndex:pageIndex WithSortingMethodString:@""];
        }else{
            [self dataIsReadyWithArr:nil];
            return;
        }
        NSMutableArray* returnArray = [[NSMutableArray alloc] initWithArray:array];
        dispatch_async(dispatch_get_main_queue(), ^{
            //update the UI in main queue;
            [self dataIsReadyWithArr:returnArray];
        });
    });
    dispatch_release(downloadQueue); //won’t actually go away until queue is empty
}
-(void)AuthorizeDelegateSuccess
{
    
}

-(void)AuthorizeDelegateFail
{
    
}


@end
