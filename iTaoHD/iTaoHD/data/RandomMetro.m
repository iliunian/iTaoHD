//
//  RandomMetro.m
//  iMiniTao
//
//  Created by liunian on 13-8-20.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "RandomMetro.h"

@interface RandomMetro (){
    NSInteger   _index;
}
@property (nonatomic, retain) NSArray *colors;

@end

static RandomMetro * sharedManager;
@implementation RandomMetro

- (id)init{
    if (self = [super init]) {
        _index = 0;
        UIColor *color1 = [UIColor getColor:@"57DAEB"];
        UIColor *color2 = [UIColor getColor:@"1463AB"];
        UIColor *color3 = [UIColor getColor:@"ECA841"];
        UIColor *color4 = [UIColor getColor:@"FFB538"];
        UIColor *color5 = [UIColor getColor:@"9BD84D"];
        [UIColor flatDarkGreenColor];
        _colors = [NSArray arrayWithObjects:color1,color2,color3,color4,color5, nil];
        
    }
    return self;
}

+ (RandomMetro *)sharedManager{
    if (!sharedManager) {
        sharedManager = [[RandomMetro alloc] init];
    }
    return sharedManager;
}

- (UIColor *)color{
    return [UIColor randomFlatColor];
    _index = arc4random() % 5;
    return [_colors objectAtIndex:_index];
}
@end
