//
//  CommentRequest.h
//  iTaoHD
//
//  Created by liunian on 13-12-4.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"


@interface CommentRequest : NSObject
- (void)sendCommentWithItem:(CuzyTBKItem *)item
                    message:(NSString *)message
                author_name:(NSString *)author_name
                 author_url:(NSString *)author_url
               withDelegate:(id<ReqDelegate>)delegate;

- (void)synchronizeItem:(NSArray *)items withDelegate:(id<ReqDelegate>)delegate;
- (void)getCommentWithItemID:(NSString *)itemID withDelegate:(id<ReqDelegate>)delegate;
@end
