//
//  MeSource.h
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Source.h"
@class UserSourceEntity;
@class CustomSourceEntity;
@interface MeSource : NSObject
@property (nonatomic, retain) NSString *sid;
@property (nonatomic, retain) NSString *themeid;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) ITEMTYPE itemtype;
@property (nonatomic, assign) BOOL  custom;

- (id)initWithSource:(Source *)source;
- (id)initWithUserSourceEntity:(UserSourceEntity *)entity;
- (id)initWithCustomSourceEntity:(CustomSourceEntity *)entity;
@end
