//
//  ReqDelegate.h
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ReqDelegate <NSObject>
@optional
- (void)finishErrorReq:(id)Req errorCode:(int) errorCode message:(NSString *)message;
//Source
- (void)finishSourceReq:(id)Req sources:(NSArray *)sources;
//HotSource
- (void)finishSourceReq:(id)Req hotSources:(NSArray *)hotSources;

- (void)finishSourceReq:(id)Req itemID:(NSString *)itemid comments:(NSArray *)comments;
@end
