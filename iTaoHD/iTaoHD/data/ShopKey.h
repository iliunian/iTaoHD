//
//  ShopKey.h
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopKey : NSObject
@property (nonatomic, assign) NSInteger fid;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *key;
@end
