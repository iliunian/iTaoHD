//
//  ApplicationManager.h
//  iMiniTao
//
//  Created by liu nian on 13-8-15.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKEnvObserverCenter.h"
@protocol ApplicationManagerDelegate <NSObject>
@optional

- (void)mkManagerDesktopBackgroundImageChanged:(NSString *)imageName;

@end

@interface ApplicationManager : NSObject
+ (ApplicationManager *)sharedManager;
- (void)addObserver:(id<ApplicationManagerDelegate>)observer;
- (void)removeObserver:(id<ApplicationManagerDelegate>)observer;
//是否第一次启动
- (BOOL)isFirstLanuch;
//标记
- (void)flagLanuch;

- (NSString *)backgroundImage;
- (void)setBackgroundImage:(NSString *)imageName;
@end
