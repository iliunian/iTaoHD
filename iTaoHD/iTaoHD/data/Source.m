//
//  Source.m
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "Source.h"

@implementation Source
+ (id)createWithDict:(NSDictionary *)dict
{
    return [[Source alloc] initWithDict:dict];
}

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        if (dict) {
            self.sid = BUGetObjFromDict(@"id", dict, [NSString class]);
            self.themeid = BUGetObjFromDict(@"themeid", dict, [NSString class]);
            self.name = BUGetObjFromDict(@"name", dict, [NSString class]);
            self.description = BUGetObjFromDict(@"description", dict, [NSString class]);
            self.icon = BUGetObjFromDict(@"icon", dict, [NSString class]);
            self.type = BUGetObjFromDict(@"type", dict, [NSString class]);
            self.itemtype =[BUGetObjFromDict(@"itemtype", dict, [NSString class]) integerValue];
        }
    }
    return self;
}

#pragma mark - NSCoding
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.sid = [aDecoder decodeObjectForKey:@"id"];
        self.themeid = [aDecoder decodeObjectForKey:@"themeid"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.description = [aDecoder decodeObjectForKey:@"description"];
        self.icon = [aDecoder decodeObjectForKey:@"icon"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
        self.itemtype = [[aDecoder decodeObjectForKey:@"itemtype"] integerValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.sid forKey:@"id"];
    [aCoder encodeObject:self.themeid forKey:@"themeid"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.description forKey:@"description"];
    [aCoder encodeObject:self.icon forKey:@"icon"];
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.itemtype] forKey:@"itemtype"];

}
@end
