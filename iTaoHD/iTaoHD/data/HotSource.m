//
//  HotSource.m
//  iMiniTao
//
//  Created by liunian on 13-8-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "HotSource.h"

@implementation HotSource

+ (id)createWithDict:(NSDictionary *)dict
{
    return [[HotSource alloc] initWithDict:dict];
}

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        if (dict) {
            self.hid = BUGetObjFromDict(@"id", dict, [NSString class]);
            self.title = BUGetObjFromDict(@"title", dict, [NSString class]);
            self.image = BUGetObjFromDict(@"image", dict, [NSString class]);
            self.type = [BUGetObjFromDict(@"type", dict, [NSString class]) intValue];
            self.url = BUGetObjFromDict(@"url", dict, [NSString class]);
            self.itemid = BUGetObjFromDict(@"itemid", dict, [NSString class]);
        }
    }
    return self;
}

@end
