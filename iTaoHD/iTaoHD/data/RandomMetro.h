//
//  RandomMetro.h
//  iMiniTao
//
//  Created by liunian on 13-8-20.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomMetro : NSObject

+ (RandomMetro *)sharedManager;
- (UIColor *)color;
@end
