//
//  ApplicationManager.m
//  iMiniTao
//
//  Created by liu nian on 13-8-15.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ApplicationManager.h"


#define KISFIRSTLANUCH  @"isFirstLanuch"

#define KBACKGROUNDIMAGE  @"KBACKGROUNDIMAGE"
@interface ApplicationManager (){
    MKEnvObserverCenter    *_obCenter;
}

@end
static ApplicationManager * sharedManager;
@implementation ApplicationManager

+ (ApplicationManager *)sharedManager{
    if (!sharedManager) {
        sharedManager = [[ApplicationManager alloc] init];
    }
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _obCenter = [[MKEnvObserverCenter alloc] init];
    }
    return self;
}

/*
 [[NSUserDefaults standardUserDefaults] setObject:newNum forKey:@"DefaultImageId"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 */

- (void)addObserver:(id<ApplicationManagerDelegate>)observer
{
    [_obCenter addEnvObserver:observer];
}

- (void)removeObserver:(id<ApplicationManagerDelegate>)observer
{
    [_obCenter removeEnvObserver:observer];
}


- (BOOL)isFirstLanuch{
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:KISFIRSTLANUCH];
    if (string == nil) {
        return YES;
    }
    return NO;
}

- (void)flagLanuch{
    if ([self isFirstLanuch]) {
        [[NSUserDefaults standardUserDefaults] setValue:KISFIRSTLANUCH forKey:KISFIRSTLANUCH];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSString *)backgroundImage{
    NSString *imageName = [[NSUserDefaults standardUserDefaults] objectForKey:KBACKGROUNDIMAGE];
    if (imageName == nil) {
        return nil;
    }
    return imageName;
}

- (void)setBackgroundImage:(NSString *)imageName{
    if (imageName == nil) {
        return;
    }
    
    if ([self backgroundImage] && [[self backgroundImage] isEqualToString:imageName]) {
        return;
    }
    [[NSUserDefaults standardUserDefaults] setValue:imageName forKey:KBACKGROUNDIMAGE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_obCenter noticeObervers:@selector(mkManagerDesktopBackgroundImageChanged:)
                   withObject:imageName];
}
@end
