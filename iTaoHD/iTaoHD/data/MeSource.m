//
//  MeSource.m
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "MeSource.h"
#import "UserSourceEntity.h"
#import "CustomSourceEntity.h"

@implementation MeSource

- (id)initWithSource:(Source *)source{
    self = [super init];
    if (self) {
        if (source) {
            self.sid = source.sid;
            self.themeid = source.themeid;
            self.name = source.name;
            self.description = source.description;
            self.icon = source.icon;
            self.custom = NO;
            self.index = 0;
            self.itemtype = source.itemtype;
        }
    }
    return self;
}

- (id)initWithUserSourceEntity:(UserSourceEntity *)entity{
    self = [super init];
    if (self) {
        if (entity) {
            self.sid = [NSString stringWithFormat:@"%d",[entity.sid integerValue]];
            self.themeid = [NSString stringWithFormat:@"%d",[entity.themeid integerValue]];
            self.name = entity.name;
            self.description = entity.description;
            self.icon = entity.icon;
            self.custom = [entity.isCustom boolValue];
            self.index = [entity.sortid integerValue];
            self.itemtype = [entity.itemtype integerValue];
        }
    }
    return self;
}
- (id)initWithCustomSourceEntity:(CustomSourceEntity *)entity{
    self = [super init];
    if (self) {
        if (entity) {
            self.sid = @"0";
            self.themeid = @"0";
            self.name = entity.keyword;
            self.description = nil;
            self.icon = entity.icon;
            self.custom = YES;
            self.index = 0;
            self.itemtype = ITEMTYPEKEYWORD;
        }
    }
    return self;
}
@end
