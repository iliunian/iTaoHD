//
//  SAEComment.m
//  iTaoHD
//
//  Created by liunian on 13-12-9.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "SAEComment.h"
#import "HttpRequest.h"

#define kSAEComment_insert  @"http://imitao.sinaapp.com/api/review/insert/"
#define kSAEComment_list  @"http://imitao.sinaapp.com/api/review/list/"
@implementation SAEComment
- (void)insertSAECommentWithItemID:(NSString *)itemid
                           message:(NSString *)message
                       author_name:(NSString *)author_name
                        avatar_url:(NSString *)avatar_url
                        author_url:(NSString *)author_url
                      withDelegate:(id<ReqDelegate>)delegate{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(itemid, @"itemid", param);
    SET_PARAM(message, @"content", param);
    SET_PARAM(author_name, @"name", param);
    SET_PARAM(avatar_url, @"avatar_url", param);
    SET_PARAM(author_url, @"url", param);
    
    
    [HttpRequest requestWithURLStr:kSAEComment_insert
                             param:param
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       BMLog(@"111111:%@",request.responseString);
                       if ([HttpRequest isRequestSuccess:request]) {
                           
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                       BMLog(@"2222:%@",request.responseString);
//                       if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
//                           [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
//                       }
                   }];

    
}

- (void)listSAECommentWithItemID:(NSString *)itemid
                    withDelegate:(id<ReqDelegate>)delegate{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(itemid, @"itemid", param);
    SET_PARAM(@"50", @"count", param);
    SET_PARAM(@"asc", @"ord", param);
    SET_PARAM(@"id", @"by", param);
    [HttpRequest requestWithURLStr:kSAEComment_list
                             param:param
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       BMLog(@"111111:%@",request.responseString);
                       if ([HttpRequest isRequestSuccess:request]) {
                           
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                   }];
}

@end
