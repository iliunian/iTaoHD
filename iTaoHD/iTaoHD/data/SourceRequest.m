//
//  SourceRequest.m
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "SourceRequest.h"
#import "HttpRequest.h"

#define apiSource @"http://iminitao.duapp.com/itao_hd/source_v2.php"
@interface SourceRequest ()
@property (nonatomic , assign) id<ReqDelegate>delegate;
@end

@implementation SourceRequest

- (void)requestAllSourcesWithDelegate:(id<ReqDelegate>)delegate;{
    _delegate = nil;
    _delegate = delegate;
    [HttpRequest requestWithURLStr:apiSource
                             param:nil
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       
                       if ([HttpRequest isRequestSuccess:request]) {
                           NSMutableArray *rstArray = [NSMutableArray arrayWithCapacity:0];
                           NSArray *sources = [jsonDict objectForKey:@"data"];
                           
                           if (isArrayWithCountMoreThan0(sources)) {
                               for (NSDictionary *dic in sources) {
                                   if (isDictWithCountMoreThan0(dic)) {
                                        Source *s = [Source createWithDict:dic];
                                       if (s.itemtype !=ITEMTYPENONE) {
                                           [rstArray addObject:s];
                                       }
                                   }
                               }
                               
                               if ([_delegate respondsToSelector:@selector(finishSourceReq:sources:)]) {
                                   [_delegate finishSourceReq:self sources:rstArray];
                               }
                               
                               
                           }else{
                               if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                                   [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                               }
                           }
                       }
    } failedBlock:^(ASIHTTPRequest *request) {
        if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
            [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
        }
    }];
}
@end
