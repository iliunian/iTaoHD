//
//  Comment.h
//  iTaoHD
//
//  Created by liu nian on 13-12-9.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *avatar_url;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *created_at;
@end
