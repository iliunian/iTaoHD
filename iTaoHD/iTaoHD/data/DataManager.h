//
//  DataManager.h
//  wine
//
//  Created by LiZujian on 13-6-23.
//  Copyright (c) 2013年 LiZujian. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 picsize: 可以定制返回图片的大小，注意图片越大，获取图片的时间越长。默认返回的图片大小为200*200
 600x600  400x400  360x360  350x350 320x320  310x310
 300x300  290x290   270x270  250x250 240x240 230x230
 220x220  210x210  200x200   190x190  180x180 170x170
 160x160  130x130   120x120  110x110   100x100 90x90
 80x80      70x70      60x60      40x40
 */

typedef enum
{
    PicSize360 = 0,
    PicSize400,
    PicSize600
} PicSizeType;

@protocol DataManagerDelegate <NSObject>

-(void) updateViewForSuccess:(id)dataModel;
-(void) updateViewForError:(NSError *)errorInfo;
@end

@interface DataManager : NSObject
{
    id<DataManagerDelegate> _delegate;
}

@property (nonatomic, assign) id<DataManagerDelegate> delegate;
@property (nonatomic, assign) BOOL isIphone5;
AS_SINGLETON(DataManager)

-(void)requstTBKItemsWithKeyWord:(NSString*)searchKey
                     WithThemeID:(NSString *)themeId
                 WithPicSizeType:(PicSizeType)PSType
                        WithSort:(NSString *)sort
                   WithPageIndex:(NSInteger) pageIndex
                    withDelegate:(id<DataManagerDelegate>)aDelegate;
- (void)requstTBKItemsWithKeyWord:(NSString*)searchKey WithThemeID:(NSString *)themeId WithPicSizeType:(PicSizeType)PSType WithPageIndex:(NSInteger) pageIndex withDelegate:(id<DataManagerDelegate>)aDelegate;
- (void)requstTBKItemsWithKeyWord:(NSString*)searchKey WithThemeID:(NSString *)themeId WithPageIndex:(NSInteger) pageIndex withDelegate:(id<DataManagerDelegate>)aDelegate;

- (void)requestTBKShopItemsWithKeyword:(NSString*)searchKey WithPageIndex:(NSInteger) pageIndex withDelegate:(id<DataManagerDelegate>)aDelegate;

@end
