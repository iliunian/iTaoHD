//
//  SAEComment.h
//  iTaoHD
//
//  Created by liunian on 13-12-9.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"

@interface SAEComment : NSObject
- (void)insertSAECommentWithItemID:(NSString *)itemid
                           message:(NSString *)message
                       author_name:(NSString *)author_name
                        avatar_url:(NSString *)avatar_url
                        author_url:(NSString *)author_url
                      withDelegate:(id<ReqDelegate>)delegate;

- (void)listSAECommentWithItemID:(NSString *)itemid
                    withDelegate:(id<ReqDelegate>)delegate;
@end
