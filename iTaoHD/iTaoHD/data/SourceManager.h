//
//  SourceManager.h
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKEnvObserverCenter.h"

@class MeSource;
@protocol SourceManagerDelegate <NSObject>
@optional

- (void)mkManagerMeSourcesDidFinishedWithSources:(NSMutableArray *)meSources;
//所有频道请求 回调
- (void)mkManagerAllSourcesRequestDidFinishedWithSources:(NSArray *)sources;

- (void)mkManagerHotSourcesDidFinishedWithSources:(NSArray *)hotSources;

- (void)mkManagerAllSourcesRequestDidFailed;


- (void)mkModelManagerSubscribeSourcesAtIndex:(NSNumber *)index;
- (void)mkModelManagerUnsubscribeSourcesAtIndex:(NSNumber *)index;

- (void)mkManagerCustomSourcesDidFinishedWithSources:(NSMutableArray *)customSources;

@end

@interface SourceManager : NSObject
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSMutableArray *meSources;

+ (SourceManager *)sharedManager;

- (void)addObserver:(id<SourceManagerDelegate>)observer;
- (void)removeObserver:(id<SourceManagerDelegate>)observer;

- (void)addSource:(MeSource *)source;
- (void)requestAllSources;
- (void)getUserSources;

- (BOOL)removeMeSource:(MeSource *)obj;
- (BOOL)removeTheMeSource:(MeSource *)obj;

- (void)requestHotSources;
- (void)reorderMeSources;

- (BOOL)isExistKeyword:(NSString *)keyword;
- (void)addCustomSourceKeyword:(NSString *)keyword icon:(NSString *)icon;
- (void)getCustomSource;
- (void)addCustomSourceKeyword:(NSString *)keyword;
- (void)removeCustomSourceKeyWord:(NSString *)keyword;
@end
