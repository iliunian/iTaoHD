//
//  FavoritManager.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FavoritManagerDelegate <NSObject>
- (void)mkManagerFavoritesDidFinishedWithFavorites:(NSMutableArray *)favorites;
@end
@interface FavoritManager : NSObject
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSMutableArray *favorites;

+ (FavoritManager *)sharedManager;

- (void)addObserver:(id<FavoritManagerDelegate>)observer;
- (void)removeObserver:(id<FavoritManagerDelegate>)observer;

- (void )getFavorites;
- (BOOL)isFavorit:(CuzyTBKItem *)item;
- (void)reorderFavorites;
- (void)favoritItem:(CuzyTBKItem *)item;
- (void)removeItem:(CuzyTBKItem *)item;
@end
