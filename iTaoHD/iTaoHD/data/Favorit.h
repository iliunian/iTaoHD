//
//  Favorit.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Favorit : NSObject
@property (nonatomic, assign) NSInteger    index;
@property (nonatomic, retain) CuzyTBKItem *item;

@end
