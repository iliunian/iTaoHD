//
//  CommentRequest.m
//  iTaoHD
//
//  Created by liunian on 13-12-4.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CommentRequest.h"
#import "HttpRequest.h"
#import "OFUtil.h"
#import "Comment.h"

#define kCommentSendAPI @"http://api.duoshuo.com/posts/create.json" //发布评论
#define kItemSendAPI @"http://api.duoshuo.com/threads/import.json"//同步文章
#define kItemImportAPI  @"http://www.iliunian.com/api/import_item.php"

#define kListPostsAPI  @"http://itao.duoshuo.com/api/threads/listPosts.json"
#define kDuoshuoShort_name @"itao"
#define kDuoshuoSecret  @"41e6b92f364f4c0a109f5f4215b31048"


@interface CommentRequest ()
@property (nonatomic , assign) id<ReqDelegate>delegate;
@end
@implementation CommentRequest

- (void)synchronizeItem:(NSArray *)items withDelegate:(id<ReqDelegate>)delegate{
    _delegate = nil;
    _delegate = delegate;

    NSMutableArray  *threads = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (CuzyTBKItem *item in items) {
        NSString *itemIDStr = [NSString stringWithFormat:@"%d",[item.itemID intValue]];
        NSString *clickURL = [@"http://" stringByAppendingFormat:@"%@",item.itemClickURLString];
        NSMutableDictionary *thread = [[NSMutableDictionary alloc] initWithCapacity:0];
        SET_PARAM(itemIDStr, @"thread_key", thread);
        SET_PARAM(item.itemName, @"title", thread);
        SET_PARAM(clickURL, @"url", thread);
        
        [threads addObject:thread];
    }
    
    NSString *threadsJson = [threads JSONRepresentation];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(threadsJson, @"threads", param);
    
    [HttpRequest requestWithURLStr:kItemImportAPI
                             param:param
                        httpMethod:HttpMethodPost
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       BMLog(@"111111:%@",request.responseString);
                       if ([HttpRequest isRequestSuccess:request]) {
                           
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                       BMLog(@"2222:%@",request.responseString);
                       if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                           [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                       }
                   }];
}

- (void)sendCommentWithItem:(CuzyTBKItem *)item
                    message:(NSString *)message
                author_name:(NSString *)author_name
                 author_url:(NSString *)author_url
               withDelegate:(id<ReqDelegate>)delegate{
    _delegate = nil;
    _delegate = delegate;
    BMLog(@"%@",item.description);
    NSString *itemIDStr = [NSString stringWithFormat:@"%d",[item.itemID intValue]];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(kDuoshuoShort_name, @"short_name", param);
    SET_PARAM(kDuoshuoSecret, @"secret", param);
    SET_PARAM(message, @"message", param);
    
    SET_PARAM(itemIDStr, @"thread_key", param);
    SET_PARAM(author_name, @"author_name", param);
    SET_PARAM(author_url, @"author_url", param);
    SET_PARAM(@"popln@163.com", @"author_email", param);
    [HttpRequest requestWithURLStr:kCommentSendAPI
                             param:param
                        httpMethod:HttpMethodPost
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       BMLog(@"%@",jsonDict);
                       if ([HttpRequest isRequestSuccess:request]) {
                           
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                       if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                           [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                       }
                   }];
}

- (void)getCommentWithItemID:(NSString *)itemID withDelegate:(id<ReqDelegate>)delegate{
    _delegate = nil;    
    _delegate = delegate;
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(itemID, @"thread_key", param);
    
    [HttpRequest requestWithURLStr:kListPostsAPI
                             param:param
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       
       
                       
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       BMLog(@"%@",jsonDict);
                       NSMutableArray *comments = nil;
                       if ([HttpRequest isRequestSuccess:request]) {
                           
                           NSArray *response = [jsonDict objectForKey:@"response"];
                            comments = [[NSMutableArray alloc] initWithCapacity:response.count];
                           NSDictionary *parentPosts = [jsonDict objectForKey:@"parentPosts"];
                           if (response) {
                               for (NSString *key in response) {
                                   NSDictionary *item = [parentPosts objectForKey:key];
                                   Comment *comment = [[Comment alloc] init];
                                   comment.name = [[item objectForKey:@"author"] objectForKey:@"name"];
                                   if (![[[item objectForKey:@"author"] objectForKey:@"avatar_url"] isKindOfClass:[NSNull class]]) {
                                       comment.avatar_url = [[item objectForKey:@"author"] objectForKey:@"avatar_url"];
                                   }
                                   
                                   comment.url = [[item objectForKey:@"author"] objectForKey:@"url"];
                                   comment.message = [item objectForKey:@"message"];
                                   [comments addObject:comment];
                               }
                           }
                       }
                       if ([_delegate respondsToSelector:@selector(finishSourceReq:itemID:comments:)]) {
                           [_delegate finishSourceReq:self  itemID:itemID comments:comments];
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                       if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                           [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                       }
                   }];
}
@end
