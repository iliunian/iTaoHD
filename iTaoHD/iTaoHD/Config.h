//
//  Config.h
//  iMiniTao
//
//  Created by liunian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#ifndef iMiniTao_Config_h
#define iMiniTao_Config_h


#define cuzyAppkey      @"200075"
#define cuzyAppSecret   @"54b09b3306ab0f6a1a3a70342701b40c"

// ************************************//
// **************   渠道列表  ***********//
// ************************************//
#define kUmengAppstoreChannelId       @"appstore"
#define kUmeng91storeChannelId       @"91市场"
#define kUmengTongbustoreChannelId       @"tongbu"
#define kUmengWeiphoneChannelId       @"weiphone"
// ************************************//

#define kUmengChannelId       @"appstore"
#define kUmengAppKey        @"52a7f8ce56240b90300096b7"
#define kWeiboAppKey        @"2887698803"//tanshan


#define kLinkAPPSite @"http://www.iminitao.com/"
#define kLinkItunes @"https://itunes.apple.com/cn/app/tao-tao-kan/id693893823?ls=1&mt=8"
#define kAPPID  693893823

// ************************************//
// ********  Save DIR Base  ***********//
// ************************************//
#define MKDirFileCache [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"FileCache"]
#define MKCacheDir     [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define MKDocumentDir  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define kSelectedThemID @"1180"


#define kDefaultAnimationIntervalSlow     1.0f
#define kDefaultAnimationIntervalMedium   0.5f
#define kDefaultAnimationIntervalFast     0.3f

#define kDefaultBottomBarHeight           58

#define COLOR_RGB_NAV        [UIColor colorWithRed:221.0/255.0f green:221.0/255.0f blue:221.0/255.0f alpha:1.0]
#define COLOR_RGB_NAV_NIGHT  [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0]

#define COLOR_RGB_BOTTOM     [UIColor colorWithRed:234.0/255.0f green:234.0/255.0f blue:234.0/255.0f alpha:1.0]
#define COLOR_RGB_BOTTOM_NIGHT  [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0]
#endif
