//
//  FavorityEntity.h
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FavorityEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * sortid;
@property (nonatomic, retain) NSNumber * itemID;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSString * itemPrice;
@property (nonatomic, retain) NSString * tradingVolumeInThirtyDays;
@property (nonatomic, retain) NSString * itemClickURLString;
@property (nonatomic, retain) NSString * itemImageURLString;
@property (nonatomic, retain) NSString * promotionPrice;

@end
