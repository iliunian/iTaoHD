//
//  SearchCell.m
//  iMiniTao
//
//  Created by liu nian on 13-8-20.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - getter
- (UIImageView *)srcImageView{
    if (!_srcImageView) {
        _srcImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
        _srcImageView.backgroundColor = [UIColor clearColor];
        _srcImageView.contentMode = UIViewContentModeScaleAspectFill;
        _srcImageView.userInteractionEnabled = YES;
        _srcImageView.clipsToBounds = YES;
        [self.contentView addSubview:_srcImageView];
    }
    return _srcImageView;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 10, 540 - 140 ,40)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = [UIColor darkTextColor];
        _nameLabel.font = FONT_CONTENT(14.0f);
        [_nameLabel setNumberOfLines:0];
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (StrikeThroughLabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[StrikeThroughLabel alloc] initWithFrame:CGRectMake(140, 55, 100, 25)];
        _priceLabel.backgroundColor = [UIColor clearColor];
        _priceLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        _priceLabel.textColor = [UIColor flatGrayColor];
        _priceLabel.font = FONT_CONTENT(14.0f);
        [_priceLabel setNumberOfLines:1];
        _priceLabel.strikeThroughEnabled = YES;
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}
- (UILabel *)promotionPriceLabel{
    if (!_promotionPriceLabel) {
        _promotionPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.priceLabel.frame), CGRectGetMinY(self.priceLabel.frame), 100, 25)];
        _promotionPriceLabel.backgroundColor = [UIColor clearColor];
        _promotionPriceLabel.textColor = [UIColor flatRedColor];
        _promotionPriceLabel.font = FONT_CONTENT(14.0f);
        [_promotionPriceLabel setNumberOfLines:1];
        [self.contentView addSubview:_promotionPriceLabel];
    }
    return _promotionPriceLabel;
}

@end
