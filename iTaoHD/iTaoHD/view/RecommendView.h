//
//  RecommendView.h
//  Mooker
//
//  Created by Jikui Peng on 12-7-17.
//  Copyright (c) 2012年 banma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Source.h"

@protocol RecommendViewDelegate <NSObject>
@optional
- (void)showRecommendDetailWithArticle:(Source *)recArticle withRecommendId:(NSString *)recommendId;

@end

@interface RecommendView : UIView

@property (nonatomic, assign) id<RecommendViewDelegate>   delegate;
- (void)refreshData;
- (void)startAnimation;
- (void)stopAnimation;
@end

