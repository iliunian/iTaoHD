//
//  ShopCell.h
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "LolayGridViewCell.h"


#define kShopCellWidth  160
#define kShopCellHeight 200

@interface ShopCell : LolayGridViewCell
@property (nonatomic, retain) UIImageView    *containerView;
@property (nonatomic, retain) UIImageView    *channelImgView;
@property (nonatomic, retain) UILabel        *nameLabel;        //频道名
@property (nonatomic, retain) UILabel        *countLabel;
@property (nonatomic, retain) CuzyTBShopItem   *shopItem;
@end
