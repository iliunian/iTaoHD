//
//  SearchCell.h
//  iMiniTao
//
//  Created by liu nian on 13-8-20.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrikeThroughLabel.h"

#define kSearchCellHeight 120

@interface SearchCell : UITableViewCell
@property (nonatomic, retain) UILabel        *nameLabel;
@property (nonatomic, retain) UIImageView    *srcImageView;
@property (nonatomic, retain) StrikeThroughLabel        *priceLabel;
@property (nonatomic, retain) UILabel        *promotionPriceLabel;
@end
