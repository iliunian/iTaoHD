//
//  MPFlipEnumerations.h
//  MPTransition (v1.1.0)
//
//  Created by Mark Pospesel on 5/15/12.
//  Copyright (c) 2012 Mark Pospesel. All rights reserved.
//

#ifndef MPFoldTransition_MPFlipEnumerations_h
#define MPFoldTransition_MPFlipEnumerations_h

enum {
	// current view folds away into center, next view slides in flat from top & bottom
	MPFlipStyleDirectionLeft				        = 0,
    MPFlipStyleDirectionRight				        = 1,
	MPFlipStyleDirectionUp                          = 2,
    MPFlipStyleDirectionDown				        = 3,
    MPFlipStylePerspectiveDirectionLeft				= 4,
    MPFlipStylePerspectiveDirectionRight	        = 5,
	MPFlipStylePerspectiveDirectionUp               = 6,
    MPFlipStylePerspectiveDirectionDown				= 7
};
typedef NSUInteger MPFlipStyle;

#define MPFlipStyleDirectionMask	MPFlipStyleDirectionRight
#define MPFlipStyleOrientationMask	MPFlipStyleDirectionUp
#define MPFlipStylePerspectiveMask	MPFlipStylePerspectiveDirectionLeft

#endif
