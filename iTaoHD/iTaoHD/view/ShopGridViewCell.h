//
//  ShopGridViewCell.h
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "GMGridViewCell.h"

@class ShopKey;
@interface ShopGridViewCell : GMGridViewCell
@property (nonatomic, retain) UIImageView        *containView;
@property (nonatomic, retain) UIImageView    *channelImgView;
@property (nonatomic, retain) UILabel        *nameLabel;        //频道名
@property (nonatomic, retain) UILabel        *countLabel;
@property (nonatomic, retain) ShopKey   *shop;
@end
