//
//  SourceViewCell.m
//  Mooker
//
//  Created by JK.Peng on 13-1-3.
//
//

#import "SourceViewCell.h"


//频道背景颜色
#define COLOR_RGBA_SOURCE_BG  COLOR_RGBA(0, 174.0f, 238.0f, 0.6f)
#define COLOR_RGB_SOURCE_BG   COLOR_RGB(0, 174.0f, 238.0f)

#define COLOR_TABLECELL_SELECT_DAY      COLOR_RGB(234, 234, 234)
#define COLOR_TABLECELL_SELECT_NIGHT    COLOR_RGB(55, 55, 55)

@implementation SourceViewCell
@synthesize srcImageView = _srcImageView;
@synthesize nameLabel = _nameLabel;
@synthesize summaryLabel = _summaryLabel;
@synthesize subscribeImageView  = _subscribeImageView;
@synthesize delegate = _delegate;
@synthesize source = _source;

- (void)dealloc{
    _delegate = nil;
    RELEASE_SAFELY(_source);
    RELEASE_SAFELY(_srcImageView);
    RELEASE_SAFELY(_nameLabel);
    RELEASE_SAFELY(_summaryLabel);
    RELEASE_SAFELY(_subscribeImageView);
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse{
    [super prepareForReuse];
    _srcImageView.image = nil;
    _subscribeImageView.image = nil;
    _source = nil;
}

- (void)enterToChannel
{
    if ([_delegate respondsToSelector:@selector(enterChannelForSourceViewCell:)]) {
        [_delegate enterChannelForSourceViewCell:self];
    }
}

- (void)handleSubscribedSource
{
    if ([_delegate respondsToSelector:@selector(handleSubscribeForSourceViewCell:)]) {
        [_delegate handleSubscribeForSourceViewCell:self];
    }
}

#pragma mark - getter
- (UIImageView *)srcImageView{
    if (!_srcImageView) {
        _srcImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(22, 14, 65, 62)];
        _srcImageView.backgroundColor = COLOR_RGB_SOURCE_BG;
        _srcImageView.contentMode = UIViewContentModeScaleAspectFill;
        _srcImageView.userInteractionEnabled = YES;
        _srcImageView.clipsToBounds = YES;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(enterToChannel)];
		[_srcImageView addGestureRecognizer:tapGesture];
        
        [self.contentView addSubview:_srcImageView];
    }
    return _srcImageView;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 18, 540-64, 20)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = COLOR_RGB(0, 0, 0);
        _nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UILabel *)summaryLabel{
    if (!_summaryLabel) {
        _summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 38, 540-64, 20)];
        _summaryLabel.backgroundColor = [UIColor clearColor];
        _summaryLabel.textColor = COLOR_RGB(136.0f, 136.0f, 136.0f);
        _summaryLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11];
        [self addSubview:_summaryLabel];
    }
    return _summaryLabel;
}

- (UIImageView *)subscribeImageView{
    if (!_subscribeImageView) {
        UIImage  *img = IMGNAMED(@"bg_subscribe.png");
        _subscribeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(540-img.size.width-10, floorf((90-img.size.height)/2), img.size.width, img.size.height)];
        _subscribeImageView.backgroundColor = [UIColor clearColor];
        _subscribeImageView.contentMode = UIViewContentModeScaleAspectFill;
        _subscribeImageView.clipsToBounds = YES;
        _subscribeImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handleSubscribedSource)];
		[_subscribeImageView addGestureRecognizer:tapGesture];
        
        [self.contentView addSubview:_subscribeImageView];
    }
    return _subscribeImageView;
}
@end
