//
//  GroupView.m
//  iTaoHD
//
//  Created by liunian on 13-12-12.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "GroupView.h"

@implementation GroupView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self updateUI];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)updateUI{
    [self shopImageView];
    [self jDongImageView];
}

#pragma mark 
- (UIImageView *)shopImageView{
    if (!_shopImageView) {
        _shopImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 128)];
        _shopImageView.backgroundColor = COLOR_RGBA(31.0, 144.0, 202.0, 1.0);
        _shopImageView.contentMode = UIViewContentModeScaleAspectFill;
        _shopImageView.userInteractionEnabled = YES;
        _shopImageView.clipsToBounds = YES;
        
        [self addSubview:_shopImageView];
    }
    return _shopImageView;
}
#pragma mark
- (UIImageView *)jDongImageView{
    if (!_jDongImageView) {
        _jDongImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.shopImageView.frame) + 10, self.bounds.size.width, 128)];
        _jDongImageView.backgroundColor = COLOR_RGBA(31.0, 144.0, 202.0, 1.0);
        _jDongImageView.contentMode = UIViewContentModeScaleAspectFill;
        _jDongImageView.userInteractionEnabled = YES;
        _jDongImageView.clipsToBounds = YES;
        
        [self addSubview:_jDongImageView];
    }
    return _jDongImageView;
}
@end
