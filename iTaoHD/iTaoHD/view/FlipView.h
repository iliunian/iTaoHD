//
//  FlipView.h
//  MoKe
//
//  Created by hua zhang on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlipView;
@protocol FlipViewDataSource <NSObject>
- (NSInteger)numberOfSectionsInFlipView:(FlipView *)flipView;
- (NSInteger)numberOfPagesInFlipView:(FlipView *)flipView inSection:(NSInteger)section;
- (UIView *)flipView:(FlipView *)flipView pageInSectoin:(NSInteger)section atIndex:(NSInteger)index;
- (UIView *)flipView:(FlipView *)flipView bottomPageInSectoin:(NSInteger)section;
@end

@protocol FlipViewDelegate <NSObject>
//notify flip is done, index is the current page number.
- (void)flipView:(FlipView *)flipView flipDoneForPageInSectoin:(NSInteger)section atIndex:(NSInteger)index;

@end


//判断翻转方向（左/右）
typedef enum {
    FlipViewDirectionNone,
	FlipViewDirectionLeft,
	FlipViewDirectionRight
} FlipViewDirection;


typedef enum {
	FlipViewMoveTypeFromLeftToRight,
	FlipViewMoveTypeFromRightToLeft,
    FlipViewMoveTypeFromTopToBottom,
    FlipViewMoveTypeFromBottomToTop
} FlipViewMoveType;

@interface FlipView : UIView <UIGestureRecognizerDelegate>{
    
    BOOL              _setNewViewOnFlipDone;
    FlipViewDirection _flipDirection;
}

@property (nonatomic, assign) id<FlipViewDataSource> dataSource;
@property (nonatomic, assign) id<FlipViewDelegate>   delegate;
@property (nonatomic, assign) BOOL   flipEnabled;
@property (nonatomic, assign) NSInteger numberOfSections;
@property (nonatomic, assign) NSInteger currentSection;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL   animating;
@property (nonatomic, retain) UIImageView *bgImageView;

- (void)reloadData;
- (void)refreshData;
- (void)flipToSection:(NSInteger)section atIndex:(NSInteger)page animated:(BOOL)animated;
- (void)flipToSection:(NSInteger)section;
@end
