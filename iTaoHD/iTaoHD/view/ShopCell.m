//
//  ShopCell.m
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ShopCell.h"
#import "UIImage+Circle.h"

@implementation ShopCell

- (id)initWithFrame:(CGRect)inRect reuseIdentifier:(NSString*)inReuseIdentifier;
{
    self = [super initWithFrame:inRect reuseIdentifier:inReuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _channelImgView.image = nil;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat  bottomHeight = 30;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        bottomHeight = 34;
    }
    
    self.channelImgView.frame = CGRectMake(30, 60, 100, 100);
    self.countLabel.frame = CGRectMake(10, self.bounds.size.height - 30, self.bounds.size.width, 20);
}


- (void)updateUI{

    UIImage *image = [IMGNAMED(@"wall_bg_shadow.png") resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10,15, 10)];
    [self.containerView setImage:image];

    __block UIImageView *channelImageView_Block = self.channelImgView;
    [self.channelImgView setImageWithURL:[NSURL URLWithString:self.shopItem.shopLogoPicture] placeholderImage:IMGNAMED(@"")
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                   if (image) {
                                       UIImage *theimage = [image circleImageWithSize:channelImageView_Block.frame.size.width];
                                       [channelImageView_Block setImage:theimage];
                                   }
    }];
    
    [self.nameLabel setText:self.shopItem.shopNickName];
    [self.countLabel setText:[NSString stringWithFormat:@"商品总数:%@",self.shopItem.shopAuctionCount]];
}

- (void)setShopItem:(CuzyTBShopItem *)shopItem{
    if (_shopItem != shopItem) {
        _shopItem = shopItem;
        [self updateUI];
    }
}
#pragma mark - getter
- (UIImageView *)containerView{
    if (!_containerView) {
        _containerView = [[UIImageView alloc] initWithFrame:self.bounds];
        [_containerView setBackgroundColor:[UIColor clearColor]];
        _containerView.userInteractionEnabled = YES;
        _containerView.clipsToBounds = YES;
        [self addSubview:_containerView];
    }
    return _containerView;
}
- (UIImageView *)channelImgView{
    if (!_channelImgView) {
        _channelImgView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 40, 100, 100)];
        _channelImgView.backgroundColor = [UIColor clearColor];
        _channelImgView.contentMode = UIViewContentModeScaleAspectFit;
        _channelImgView.userInteractionEnabled = YES;
        _channelImgView.clipsToBounds = YES;
        
        [self.containerView addSubview:_channelImgView];
    }
    return _channelImgView;
}

- (UILabel *)countLabel{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.bounds.size.height - 30, self.bounds.size.width - 20, 20)];
        _countLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|
        UIViewAutoresizingFlexibleHeight;
        _countLabel.backgroundColor = [UIColor clearColor];
        _countLabel.textColor = [UIColor flatBlackColor];
        _countLabel.font =  FONT_NUMBER(16.0f);
        _countLabel.textAlignment = ALIGN_LEFT;
        [self.containerView addSubview:_countLabel];
    }
    return _countLabel;
}

- (UILabel *)nameLabel{      //sourceButton标题
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.bounds.size.width - 20, 40)];
        _nameLabel.backgroundColor = [UIColor clearColor];
//        _nameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        _nameLabel.textColor = [UIColor flatBlackColor];
        _nameLabel.font =  FONT_CONTENT(18.0f);
        _nameLabel.numberOfLines = 0;
        _nameLabel.textAlignment = ALIGNTYPE_LEFT;
        [self.containerView addSubview:_nameLabel];
    }
    return _nameLabel;
}

@end
