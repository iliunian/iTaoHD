//
//  SourceView.m
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "SourceView.h"
#import "GMGridView.h"
#import "MKGridViewCell.h"
#import "RecommendView.h"
#import "GroupView.h"
#import "MBProgressHUD.h"
#import "MKNavigationController.h"
#import "MKPageControl.h"
#import "SourceManager.h"
#import "MainViewController.h"
#import "SourceViewController.h"
#import "FavoritesController.h"
#import "OptionViewController.h"
#import "ListViewController.h"
#import "ShopListController.h"
#import "SearchViewController.h"
#define kTagSrcBtnsStart    kTagStart_01+0

#define kIntSrcBtnsPerPage  12

#define kSizeSVButtonWidth   40
#define kSizeSVButtonHeight  40


@interface SourceView ()<GMGridViewDataSource,GMGridViewDelegate,GMGridViewSortingDelegate,SourceManagerDelegate,ApplicationManagerDelegate,RecommendViewDelegate,SourceViewControllerDelegate>{
    BOOL     _isFirst;
}
@property (nonatomic, retain) UIImageView       *bgImageView;
@property (nonatomic, retain) UIImageView       *logoImageView;
@property (nonatomic, retain) UIImageView       *btnBgImageView;
@property (nonatomic, retain) UIImageView       *shadowImageView;
@property (nonatomic, retain) UIButton          *btnCollect;
@property (nonatomic, retain) MKPageControl     *pageControl;

@property (nonatomic, assign) NSInteger         currentPage;
@property (nonatomic, retain) GMGridView        *gmGridView;
@property (nonatomic, retain) RecommendView     *recommendView;
@property (nonatomic, retain) GroupView         *groupView;
@property (nonatomic, assign) NSInteger         focusCount;
@property (nonatomic, retain) MBProgressHUD     *progressHUD;

@property (nonatomic, retain) MKNavigationController   *downloadNavigationController;

@end

@implementation SourceView
@synthesize logoImageView = _logoImageView;
@synthesize btnBgImageView = _btnBgImageView;
@synthesize btnCollect = _btnCollect;
@synthesize isEditing = _isEditing;
@synthesize pageControl = _pageControl;
@synthesize totalPage = _totalPage;
@synthesize currentPage = _currentPage;
@synthesize gmGridView = _gmGridView;
@synthesize recommendView = _recommendView;
@synthesize focusCount = _focusCount;
@synthesize progressHUD = _progressHUD;
@synthesize downloadNavigationController = _downloadNavigationController;
@synthesize isSourceViewShown = _isSourceViewShown;
@synthesize shadowImageView = _shadowImageView;

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [[ApplicationManager sharedManager] removeObserver:self];
    [[SourceManager sharedManager] removeObserver:self];
    
    RELEASE_SAFELY(_downloadNavigationController);
    RELEASE_SAFELY(_btnCollect);
    RELEASE_SAFELY(_logoImageView);
    RELEASE_SAFELY(_btnBgImageView);
    RELEASE_SAFELY(_pageControl);
    RELEASE_SAFELY(_gmGridView);
    RELEASE_SAFELY(_recommendView);
    RELEASE_SAFELY(_progressHUD);
    RELEASE_SAFELY(_shadowImageView);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _isFirst = YES;
        
        self.opaque = NO;
        self.backgroundColor = [UIColor colorWithPatternImage:IMGNAMED(@"bg_sourceview.png")];

        [[SourceManager sharedManager] addObserver:self];
        [[SourceManager sharedManager] getUserSources];
        
        [[ApplicationManager sharedManager] addObserver:self];
        [self initBackground];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appNotification:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    return self;
}
- (void)initBackground{
    
    if ([[ApplicationManager sharedManager] backgroundImage]) {
        [self.bgImageView setImage:IMGNAMED([[ApplicationManager sharedManager] backgroundImage])];
    }else{
        NSInteger i = arc4random() % 14;
        NSString *imageName = [NSString stringWithFormat:@"bg_source_%d.jpg",i];
        
        UIImage *image_bg = nil;
        image_bg = IMGNAMED(imageName);
        [self.bgImageView setImage:image_bg];
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.logoImageView.frame = CGRectMake(7, 10, self.logoImageView.bounds.size.width, self.logoImageView.bounds.size.height);
    self.btnBgImageView.frame = CGRectMake(self.bounds.size.width-self.btnBgImageView.bounds.size.width-10, floorf(self.logoImageView.center.y-self.btnBgImageView.bounds.size.height/2), self.btnBgImageView.bounds.size.width, self.btnBgImageView.bounds.size.height);
    
    CGFloat  pageSize = 10.0f;
    CGFloat  centerX = self.bounds.size.width/2;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        self.recommendView.frame = CGRectMake(5, 80, 502, 267);

        self.gmGridView.frame = CGRectMake(0, self.recommendView.frame.origin.y+self.recommendView.bounds.size.height, self.bounds.size.width, 640);
        pageSize = 7.0f;          
    } else {
        self.recommendView.frame = CGRectMake(5, 80, 502, 267);
        self.groupView.frame = CGRectMake(CGRectGetMaxX(self.recommendView.frame) + 10, CGRectGetMinY(self.recommendView.frame), CGRectGetWidth(self.recommendView.frame) - 10, CGRectGetHeight(self.recommendView.frame));
        self.gmGridView.frame = CGRectMake(0,
                                           self.recommendView.frame.origin.y+self.recommendView.bounds.size.height,
                                           self.bounds.size.width,
                                           384);
        centerX = 337.0;
    }
    
    self.shadowImageView.center = CGPointMake(centerX, self.recommendView.frame.origin.y+self.recommendView.frame.size.height+3);
    
    self.pageControl.frame = CGRectMake(floorf((self.bounds.size.width-self.pageControl.bounds.size.width)/2), self.bounds.size.height-self.pageControl.bounds.size.height-pageSize, self.pageControl.bounds.size.width, self.pageControl.bounds.size.height);
    [self.gmGridView reloadData];
}

#pragma mark - button action methods
-(void)refreshSourceAction:(id)sender
{
//    if ([[MKEnvObserverCenterNetworkStatus defaultCenter] getCurrentNetworkStatus] == kNotReachable) {
//        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                          message:@"网络状态异常，请您检查网络设置"
//                                                         delegate:nil
//                                                cancelButtonTitle:@"完成"
//                                                otherButtonTitles:nil];
//        [alert show];
//        return;
//    }
//    
    
    [DataEnvironment sharedDataEnvironment].isMainViewPresentingController = YES;
    [self endEditState];
    SearchViewController   *viewController = [[SearchViewController alloc] init];
    
    MKNavigationController  *nav = [[MKNavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBarHidden = YES;
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [[[DataEnvironment sharedDataEnvironment] presentingController] presentViewController:nav
                                                                                 animated:YES
                                                                               completion:^{
                                                                                   
                                                                               }];
    RELEASE_SAFELY(viewController);
    RELEASE_SAFELY(nav);
    
    
}

-(void)infoSettingAction:(id)sender
{
    [DataEnvironment sharedDataEnvironment].isMainViewPresentingController = YES;
    [self endEditState];
    OptionViewController   *viewController = [[OptionViewController alloc] init];

    MKNavigationController  *nav = [[MKNavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBarHidden = YES;
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [[[DataEnvironment sharedDataEnvironment] presentingController] presentViewController:nav
                                                                                 animated:YES
                                                                               completion:^{
                                                                                     
                                                                                 }];
    RELEASE_SAFELY(viewController);
    RELEASE_SAFELY(nav);
    
}

- (void)showSubscribeSources:(id)sender
{
    [DataEnvironment sharedDataEnvironment].isMainViewPresentingController = YES;
    [self endEditState];
    SourceViewController  *catelog = [[SourceViewController alloc] init];
    catelog.delegate = self;
    MKNavigationController  *nav = [[MKNavigationController alloc] initWithRootViewController:catelog];
    nav.navigationBarHidden = YES;
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [[[DataEnvironment sharedDataEnvironment] mainViewController] presentViewController:nav animated:YES completion:^{
        
    }];
    RELEASE_SAFELY(catelog);
    RELEASE_SAFELY(nav);
}

- (void)enterToFocusSubjectView:(id)sender
{

    //进入收藏列表
    FavoritesController *favoritesController = [[FavoritesController alloc] init];
    [[[DataEnvironment sharedDataEnvironment] navController] pushViewController:favoritesController animated:YES];
}


- (void)pageValueChange:(id)sender{
    self.currentPage = [(MKPageControl *)sender currentPage];
    [self.gmGridView scrollRectToVisiblePageAtIndex:self.currentPage animated:YES];
    
}

#pragma mark - SrcCatelogViewDelegate
- (void)handleSource:(Source *)source isAdd:(BOOL)isAdd{
    if (!source)
        return;
    
    if (isAdd) {
        [self addSourceButton:source];
    }else{
        [self deleteSourceButton:source];
    }
}

- (void)handleMeSource:(MeSource *)source isAdd:(BOOL)isAdd{
    if (!source)
        return;
    
    if (isAdd) {
        //订阅频道
        [[SourceManager sharedManager] addSource:source];
    }else{
        //取消订阅
        [[SourceManager sharedManager] removeTheMeSource:source];
    }
}
#pragma mark - 频道处理相关 methods
- (void)addSourceButton:(Source *)source  {
    //订阅频道
    MeSource *obj = [[MeSource alloc] initWithSource:source];
    [[SourceManager sharedManager] addSource:obj];
}

- (void)deleteSourceButton:(Source *)source{
    //取消订阅
    MeSource *obj = [[MeSource alloc] initWithSource:source];
    [[SourceManager sharedManager] removeTheMeSource:obj];
}

- (void)endEditState
{
    [self.gmGridView endEditingStatus];
    self.isEditing = NO;
}
#pragma mark - 视图出现/离开 相关设置
- (void)sourceViewDidAppear
{
    [DataEnvironment sharedDataEnvironment].numOfDetailView = 0;
    [self.recommendView startAnimation];
    self.isSourceViewShown = YES;
}

- (void)sourceviewWillDisappear
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self.recommendView];
    [self.recommendView stopAnimation];
    self.isSourceViewShown = NO;
    
    if (self.gmGridView.isEditing) {
        [self endEditState];
    }
}

#pragma mark - setter methods
- (void)setCurrentPage:(NSInteger)currentPage{
    _currentPage = currentPage;
    self.pageControl.currentPageBypass = _currentPage;
    if (_totalPage<=0) {
        self.pageControl.alpha = 0.0f;
    }else{
        self.pageControl.alpha = 1.0f;
    }
}

- (void)setIsEditing:(BOOL)isEditing{
    if (_isEditing != isEditing ) {
        
        _isEditing = isEditing;
        
        if (_isEditing)
        { //当为编辑状态时，flipper禁用
            [DataEnvironment sharedDataEnvironment].mainViewController.slideEnabled = NO;
        }
        else
        { //恢复flipperalce
            [DataEnvironment sharedDataEnvironment].mainViewController.slideEnabled = YES;
        }
    }
}

- (void)setFocusCount:(NSInteger)focusCount{
    _focusCount = focusCount;
    if (_focusCount <= 0) {
        self.btnCollect.alpha = 0.0f;
        self.btnBgImageView.image = IMGNAMED(@"bg_sv_nav_line_1.png");
    }else{
        self.btnCollect.alpha = 1.0f;
        self.btnBgImageView.image = IMGNAMED(@"bg_sv_nav_line.png");
    }
    
}

#pragma mark - getter methods
- (MBProgressHUD *)progressHUD{
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithView:self];
        _progressHUD.labelFont = FONT_CONTENT(18.0);
        _progressHUD.labelText = @"正在加载中...";
        [self addSubview:_progressHUD];
    }
    return _progressHUD;
}
- (UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView  = [[UIImageView alloc] initWithFrame:self.bounds];
        _bgImageView.autoresizingMask = UIViewContentModeScaleAspectFill;
        _bgImageView.backgroundColor = [UIColor clearColor];
        _bgImageView.clipsToBounds = YES;
        
    }
    return _bgImageView;
}
- (UIImageView *)shadowImageView{
    if (!_shadowImageView) {
        UIImage *image = IMGFROMBUNDLE(@"bg_recommend_shadow.png");
        _shadowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        _shadowImageView.image = image;
        _shadowImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_shadowImageView];
    }
    return _shadowImageView;
    
}

- (UIImageView *)logoImageView{
    if (!_logoImageView) {
        UIImage *image = IMGFROMBUNDLE(@"bg_mulu_logo.png");
        _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, image.size.width, image.size.height)];
        _logoImageView.backgroundColor = [UIColor clearColor];
        _logoImageView.image = image;
        [self addSubview:_logoImageView];
    }
    return _logoImageView;
}

- (UIImageView *)btnBgImageView{
    if (!_btnBgImageView) {
        UIImage *image = IMGFROMBUNDLE(@"bg_sv_nav_line_1.png");
        _btnBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        _btnBgImageView.backgroundColor = [UIColor clearColor];
        _btnBgImageView.userInteractionEnabled = YES;
        _btnBgImageView.image = image;
        [self addSubview:_btnBgImageView];
        
        [_btnBgImageView addSubview:self.btnCollect];
        
        UIButton  *add = [UIButton buttonWithType:UIButtonTypeCustom];
        add.frame = CGRectMake(self.btnCollect.frame.origin.x+self.btnCollect.frame.size.width+26, self.btnCollect.frame.origin.y, kSizeSVButtonWidth, kSizeSVButtonHeight);
        add.backgroundColor = [UIColor clearColor];
        add.exclusiveTouch = YES;
        [add setImage:IMGFROMBUNDLE(@"btn_subscribe.png") forState:UIControlStateNormal];
        [add setImage:IMGFROMBUNDLE(@"btn_subscribe_selected.png")
             forState:UIControlStateHighlighted];
        [add addTarget:self action:@selector(showSubscribeSources:) forControlEvents:UIControlEventTouchUpInside];
        [_btnBgImageView addSubview:add];
        
        UIButton  *set = [UIButton buttonWithType:UIButtonTypeCustom];
        set.frame = CGRectMake(add.frame.origin.x+add.frame.size.width+26, add.frame.origin.y, kSizeSVButtonWidth, kSizeSVButtonHeight);
        set.backgroundColor = [UIColor clearColor];
        set.exclusiveTouch = YES;
        [set setImage:IMGFROMBUNDLE(@"btn_setting.png") forState:UIControlStateNormal];
        [set setImage:IMGFROMBUNDLE(@"btn_setting_selected.png")
             forState:UIControlStateHighlighted];
        [set addTarget:self action:@selector(infoSettingAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnBgImageView addSubview:set];
        
        UIButton  *refresh = [UIButton buttonWithType:UIButtonTypeCustom];
        refresh.frame = CGRectMake(set.frame.origin.x+set.frame.size.width+26, set.frame.origin.y, kSizeSVButtonWidth, kSizeSVButtonHeight);
        refresh.backgroundColor = [UIColor clearColor];
        refresh.exclusiveTouch = YES;
        [refresh setImage:IMGFROMBUNDLE(@"btn_mulu_refresh.png") forState:UIControlStateNormal];
        [refresh setImage:IMGFROMBUNDLE(@"btn_mulu_refresh_selected.png")
                 forState:UIControlStateHighlighted];
        [refresh addTarget:self action:@selector(refreshSourceAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnBgImageView addSubview:refresh];
        
    }
    return _btnBgImageView;
}

- (UIButton *)btnCollect{
    if (!_btnCollect) {
        _btnCollect = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnCollect.frame = CGRectMake(83, 10, kSizeSVButtonWidth, kSizeSVButtonHeight);
        _btnCollect.backgroundColor = [UIColor clearColor];
        _btnCollect.exclusiveTouch = YES;
        _btnCollect.alpha = 1.0;
        [_btnCollect setImage:IMGFROMBUNDLE(@"btn_mulu_focus.png") forState:UIControlStateNormal];
        [_btnCollect setImage:IMGFROMBUNDLE(@"btn_mulu_focus_selected.png")
                     forState:UIControlStateHighlighted];
        [_btnCollect addTarget:self action:@selector(enterToFocusSubjectView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnCollect;
}


- (MKPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[MKPageControl alloc] initWithFrame:CGRectMake(0, 0, 40, 22)];
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.isBigDot = YES;
        [_pageControl setImagePageStateNormal:IMGFROMBUNDLE(@"bg_page_normal.png")];
        [_pageControl setImagePageStateHightlighted:IMGFROMBUNDLE(@"bg_page_select.png")];
        [self addSubview:_pageControl];
        [_pageControl addTarget:self
                         action:@selector(pageValueChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _pageControl;
}

- (GMGridView *)gmGridView{
    if (!_gmGridView) {
        _gmGridView = [[GMGridView alloc] initWithFrame:CGRectMake(0, 0, 768, 630)];
        _gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _gmGridView.backgroundColor = [UIColor clearColor];
        _gmGridView.centerGrid = NO;
        _gmGridView.actionDelegate = self;
        _gmGridView.sortingDelegate = self;
        _gmGridView.dataSource = self;
        [self addSubview:_gmGridView];
    }
    return _gmGridView;
}

- (RecommendView *)recommendView{
    if (!_recommendView) {
        _recommendView = [[RecommendView alloc] initWithFrame:CGRectMake(0, 0, 368, 278)];
        _recommendView.delegate = self;
        [self addSubview:_recommendView];
    }
    return _recommendView;
}
- (GroupView *)groupView{
    if (!_groupView) {
        _groupView = [[GroupView alloc] initWithFrame:CGRectMake(388, 0, 502, 267)];

        [self addSubview:_groupView];
    }
    return _groupView;
}

#pragma mark ApplicationManagerDelegate
- (void)mkManagerDesktopBackgroundImageChanged:(NSString *)imageName{
    [self initBackground];
}

#pragma mark SourceManagerDelegate
- (void)mkManagerMeSourcesDidFinishedWithSources:(NSMutableArray *)meSources
{
    [self.gmGridView reloadData];
}

- (void)mkModelManagerSubscribeSourcesAtIndex:(NSNumber *)index
{
    [self.gmGridView insertObjectAtIndex:[index integerValue]];
}

- (void)mkModelManagerUnsubscribeSourcesAtIndex:(NSNumber *)index
{
//    NSInteger   delIndex = [index integerValue]<kIntAddSourceStartPostion?[index integerValue]+self.emptyCount:[index integerValue]+self.emptyCount+1;
    [self.gmGridView removeObjectAtIndex:[index integerValue]];
}

#pragma mark - 监听程序进入后台
- (void)appNotification:(NSNotification *)notification
{
    //横竖屏状态变化时，允许重新排版
    if (self.isEditing) {
        [self endEditState];
    }
}

#pragma mark - GMGridViewDataSource
- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [[SourceManager sharedManager].meSources count];
}

- (CGSize)sizeForItemsInGMGridView:(GMGridView *)gridView
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        return CGSizeMake(182, 200);
        
    } else {
        return CGSizeMake(160, 170);
    }
}

- (GMGridViewCell *)gmGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    
    CGSize size = [self sizeForItemsInGMGridView:gridView];
    
    MKGridViewCell *cell = (MKGridViewCell *)[gridView dequeueReusableCell];
    
    if (!cell) {
        cell = [[MKGridViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    cell.frame = CGRectMake(0, 0, size.width, size.height);
    NSArray * subSources = [SourceManager sharedManager].meSources;
    cell.source = [subSources objectAtIndex:index];
    return cell;
}

- (void)gmGridView:(GMGridView *)gridView deleteItemAtIndex:(NSInteger)index
{
    NSMutableArray * subSources = [SourceManager sharedManager].meSources;
    if (index>=0 && index < [subSources count]) {
        MeSource *source = [[SourceManager sharedManager].meSources objectAtIndex:index];
        [[SourceManager sharedManager] removeMeSource:source];
    }

}

#pragma mark - GMGridViewDelegate
- (void)gmGridViewDidEndEditing:(GMGridView *)gridView
{
    self.isEditing = NO;
}

- (void)gmGridView:(GMGridView *)gridView pageNumberInGridView:(NSInteger)pageNumber
{
    NSArray * subSources = [SourceManager sharedManager].meSources;
    // 重新计算page数
    _totalPage = ceil([subSources count]*1.0/kIntSrcBtnsPerPage);
    self.pageControl.numberOfPages = _totalPage;
    self.currentPage = pageNumber;
}

- (void)gmGridView:(GMGridView *)gridView didSelectItemAtIndex:(NSInteger)index
{
    if (!self.isSourceViewShown)
        return;
    
    MKGridViewCell  *cell = (MKGridViewCell  *)[gridView cellForItemAtIndex:index];
    if (!self.isSourceViewShown){
        return;
    }
    if (self.gmGridView.isEditing) {
        [self endEditState];
        return;
    }
    
    if (cell.source == nil)
    {
        return;
    }
    
    if (cell.source.itemtype == ITEMTYPECUZYITEM || cell.source.itemtype == ITEMTYPEKEYWORD) {
        ListViewController *listVC = [[ListViewController alloc] initWithMeSource:cell.source];
        [[[DataEnvironment sharedDataEnvironment] navController] pushViewController:listVC animated:YES];
        RELEASE_SAFELY(listVC);
    }else if (cell.source.itemtype == ITEMTYPESHOP){
        
        ShopListController *listVC = [[ShopListController alloc] init];
        [[[DataEnvironment sharedDataEnvironment] navController] pushViewController:listVC animated:YES];
    
    }else if (cell.source.itemtype == ITEMTYPEGROUP){
        
    }

    
}

- (void)delayEnterChannel:(MeSource *)source{
    [self.progressHUD stopAnimating];
//    [[DataEnvironment sharedDataEnvironment].mainViewController enterChannel:source];
}

#pragma mark - GMGridViewSortingDelegate
- (void)gmGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    self.isEditing = YES;
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.deleteButton.alpha = 1.0;
                         cell.layer.transform = CATransform3DMakeScale(1.04, 1.04, 1.0);
                     }
                     completion:nil
     ];
}

- (void)gmGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0);
                     }
                     completion:nil
     ];
}

- (void)gmGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
    NSMutableArray * subSources = [SourceManager sharedManager].meSources;
    BMLog(@"[oldIndex:%d  , newIndex:%d]",oldIndex,newIndex);
    NSInteger index1 = oldIndex;
    NSInteger index2 = newIndex;
    MeSource *object1 = [subSources objectAtIndex:index1];
    
    [[SourceManager sharedManager].meSources removeObject:object1];
    
    [[SourceManager sharedManager].meSources insertObject:object1
                                                  atIndex:index2];
    [[SourceManager sharedManager] reorderMeSources];
}

#pragma mark - RecommendViewDelegate
- (void)showRecommendDetailWithArticle:(Source *)recArticle withRecommendId:(NSString *)recommendId;
{
    if (!self.isSourceViewShown)
        return;
    
    if (self.gmGridView.isEditing) {
        [self endEditState];
        return;
    }
}


@end
