//
//  ShopGridViewCell.m
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ShopGridViewCell.h"
#import "ShopKey.h"
#import "UIImage+Circle.h"

@implementation ShopGridViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _channelImgView.image = nil;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.deleteButtonOffset = CGPointMake(self.bounds.size.width-30, 0);
    
    CGFloat  bottomHeight = 30;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        bottomHeight = 34;
    }
    
    self.channelImgView.frame = CGRectMake(30, 30, 100, 100);
//    self.countLabel.frame = CGRectMake(0, self.bounds.size.height -30, self.bounds.size.width, 30);
}


- (void)updateUI{
    UIImage *image = [IMGNAMED(@"wall_bg_shadow.png") resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10,15, 10)];
    [self.containView setImage:image];
    
    UIImage *circle_image = [IMGNAMED(self.shop.icon) circleImageWithSize:100];
    [self.channelImgView setImage:circle_image];
    [self.nameLabel setText:self.shop.name];
}

- (void)setShop:(ShopKey *)shop{
    if (_shop != shop) {
        _shop = shop;
        [self updateUI];
    }
}
#pragma mark - getter
- (UIImageView *)containView{
    if (_containView == nil) {
        _containView = [[UIImageView alloc] initWithFrame:CGRectMake(4, 2, CGRectGetWidth(self.bounds) - 8, CGRectGetHeight(self.bounds) - 4)];
        _containView.backgroundColor = [UIColor clearColor];
        _containView.userInteractionEnabled = YES;
        _containView.clipsToBounds = YES;
        [self.contentView addSubview:_containView];
    }
    return _containView;
}
- (UIImageView *)channelImgView{
    if (!_channelImgView) {
        _channelImgView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 100, 100)];
        _channelImgView.backgroundColor = [UIColor clearColor];
        _channelImgView.contentMode = UIViewContentModeCenter;
        _channelImgView.userInteractionEnabled = YES;
        _channelImgView.clipsToBounds = YES;
        
        [self.containView addSubview:_channelImgView];
    }
    return _channelImgView;
}

- (UILabel *)countLabel{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height -30, self.bounds.size.width, 30)];
        _countLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|
        UIViewAutoresizingFlexibleHeight;
        _countLabel.backgroundColor = [UIColor clearColor];
        _countLabel.textColor = COLOR_RGB(245.0f, 245.0f, 245.0f);
        _countLabel.font =  FONT_NUMBER(16.0f);
        _countLabel.textAlignment = ALIGNTYPE_CENTER;
        [self.containView addSubview:_countLabel];
    }
    return _countLabel;
}

- (UILabel *)nameLabel{      //sourceButton标题
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.bounds.size.width, 40)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font =  FONT_CONTENT(20.0f);
        _nameLabel.textAlignment = ALIGNTYPE_LEFT;
        [self.containView addSubview:_nameLabel];
    }
    return _nameLabel;
}

@end
