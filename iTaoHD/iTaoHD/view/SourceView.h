//
//  SourceView.h
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKEnvObserverCoordinator.h"

@interface SourceView : UIView {
    NSInteger           _currentPage;
    NSInteger           _totalPage;
    BOOL                _isEditing;
}

@property (nonatomic, assign,readonly) NSInteger      currentPage;
@property (nonatomic, assign,readonly) NSInteger      totalPage;
@property (nonatomic, assign) BOOL     isEditing;
@property (nonatomic, assign) BOOL     isSourceViewShown;


- (void)endEditState;
- (void)sourceViewDidAppear;
- (void)sourceviewWillDisappear;

@end
