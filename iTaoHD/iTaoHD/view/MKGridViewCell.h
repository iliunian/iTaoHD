//
//  MKGridViewCell.h
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "GMGridViewCell.h"
#import "GMGridViewCell+Extended.h"
#import "MeSource.h"

@interface MKGridViewCell : GMGridViewCell
@property (nonatomic, retain) MeSource       *source;
@property (nonatomic, retain) UIImageView    *channelImgView;
@property (nonatomic, retain) UIImageView    *bottomImgView;
@property (nonatomic, retain) UILabel        *nameLabel;        //频道名
@property (nonatomic, retain) UILabel        *countLabel;
@property (nonatomic, retain) UIImageView    *signImgView;      //微博频道未登录专用

@property (nonatomic, assign) BOOL           needFlipAnimation;

@property (nonatomic, assign) BOOL           isRefreshing;

- (void)updateData;
- (void)updateChannelImage;
@end
