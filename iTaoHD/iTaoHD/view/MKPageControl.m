//
//  MKPageControl.m
//  Mooker
//
//  Created by Jikui Peng on 12-3-15.
//  Copyright (c) 2012年 banma. All rights reserved.
//

#import "MKPageControl.h"

@interface MKPageControl ()

- (void) updateDots;
@end

@implementation MKPageControl
@synthesize imagePageStateNormal = _imagePageStateNormal;
@synthesize imagePageStateHightlighted = _imagePageStateHightlighted;
@synthesize currentPageBypass = _currentPageBypass;
@synthesize isBigDot = _isBigDot;

- (void)dealloc{
    RELEASE_SAFELY(_imagePageStateNormal);
    RELEASE_SAFELY(_imagePageStateHightlighted);
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) updateDots
{
	if (self.imagePageStateNormal || self.imagePageStateHightlighted) {
		NSArray *subviews = self.subviews;
		
        

        for(NSInteger i = 0;i<subviews.count;i++)
        {
            if ([[subviews objectAtIndex:i] isKindOfClass:[UIImageView class]]) {
                UIImageView *dot = [subviews objectAtIndex:i];
                if (self.isBigDot) {
                    CGRect  frame = dot.frame;
                    frame.size.width = 12.0f;
                    frame.size.height = 12.0f;
                    dot.frame = frame;
                }
                
                dot.image = (self.currentPage == i ? self.imagePageStateHightlighted : self.imagePageStateNormal);
            }
        }
	}
}

- (void) endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	[super endTrackingWithTouch:touch withEvent:event];
	[self updateDots];
}

#pragma mark -
#pragma mark setter 方法
- (void)setImagePageStateNormal:(UIImage *)image
{
	if (_imagePageStateNormal) {
        RELEASE_SAFELY(_imagePageStateNormal);
    }
	_imagePageStateNormal = [image retain];
	[self updateDots];
}

- (void) setImagePageStateHightlighted:(UIImage *)image
{
	if (_imagePageStateHightlighted) {
        RELEASE_SAFELY(_imagePageStateHightlighted);
    }
	_imagePageStateHightlighted = [image retain];
	[self updateDots];
}

- (void)setCurrentPageBypass:(NSInteger)aPage {
	[self setCurrentPage:aPage];
	[self updateDots];
}

- (NSInteger)currentPageBypass {
	return self.currentPage;
}

@end
