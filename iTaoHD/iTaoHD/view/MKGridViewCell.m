//
//  MKGridViewCell.m
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "MKGridViewCell.h"
#import "MPFlipTransition.h"

#define  kXPointCenterIndicatorView     self.bottomImgView.bounds.size.width-20
#define  kYPointCenterIndicatorView     self.bottomImgView.bounds.size.height/2

@interface MKGridViewCell(){
    UIImage  *_oldImage;
}
@property (nonatomic, retain) UIImage  *oldImage;
@property (nonatomic, retain) UIActivityIndicatorView    *indicatorView;
@end

@implementation MKGridViewCell

@synthesize source = _source;
@synthesize channelImgView = _channelImgView;
@synthesize bottomImgView = _bottomImgView;
@synthesize nameLabel = _nameLabel;
@synthesize countLabel = _countLabel;
@synthesize signImgView = _signImgView;
@synthesize needFlipAnimation = _needFlipAnimation;
@synthesize indicatorView = _indicatorView;
@synthesize isRefreshing = _isRefreshing;

- (void)dealloc{
    RELEASE_SAFELY(_channelImgView);
    RELEASE_SAFELY(_bottomImgView);
    RELEASE_SAFELY(_source);
    RELEASE_SAFELY(_nameLabel);
    RELEASE_SAFELY(_countLabel);
    RELEASE_SAFELY(_signImgView);
    RELEASE_SAFELY(_oldImage);
    RELEASE_SAFELY(_indicatorView);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        self.deleteButtonIcon = IMGNAMED(@"btn_close_act.png");
        self.deleteButton.alpha = 0.0f;
        self.deleteButtonOffset = CGPointMake(frame.size.width-30, 0);
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _channelImgView.image = nil;
    _needFlipAnimation = NO;
    RELEASE_SAFELY(_oldImage);
    self.isRefreshing = NO;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.deleteButtonOffset = CGPointMake(self.bounds.size.width-30, 0);
    
    CGFloat  bottomHeight = 30;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        bottomHeight = 34;
    }
    
    
    self.bottomImgView.frame = CGRectMake(0, self.bounds.size.height-bottomHeight, self.bounds.size.width, bottomHeight);
    self.channelImgView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-bottomHeight-2);
    
    
    [self relayoutItems];
    
    self.indicatorView.center = CGPointMake(kXPointCenterIndicatorView, kYPointCenterIndicatorView);
}

- (void)relayoutItems{
    if (_source) {
        self.nameLabel.text = self.source.name;
        [self updateData];
    }
}

- (void)updateData{
    self.countLabel.text = @"";
    CGSize size = [self.countLabel sizeThatFits:CGSizeMake(CGFLOAT_MIN, self.countLabel.bounds.size.height)];
    self.countLabel.frame = CGRectMake(self.bottomImgView.bounds.size.width-size.width-5, 2, size.width, self.countLabel.frame.size.height);
    self.nameLabel.frame = CGRectMake(5, 0, self.bottomImgView.bounds.size.width-size.width-10, self.nameLabel.frame.size.height);
}

- (void)updateChannelImage
{
    if (_needFlipAnimation) {
        _oldImage = self.channelImgView.image;
    }
    
    self.isRefreshing = NO;
    if (self.source.custom) {
        NSInteger index = arc4random() % 23;
        UIImage *randomImage = [UIImage imageNamed:[NSString stringWithFormat:@"ca_custom%d.png",index]];
        [self.nameLabel setFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
        [self.nameLabel setFont:FONT_CONTENT(14.0f)];
        self.nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        [self.channelImgView setImageWithURL:[NSURL URLWithString:self.source.icon] placeholderImage:randomImage];

    }else{
        [self.nameLabel setFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
        [self.nameLabel setFont:FONT_CONTENT(14.0f)];
        self.nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        [self.channelImgView setImageWithURL:[NSURL URLWithString:self.source.icon] placeholderImage:self.channelImgView.image];
    }
    
}


#pragma mark - setter
- (void)setIsRefreshing:(BOOL)isRefreshing{
    _isRefreshing = isRefreshing;
    if (_isRefreshing) {
        self.indicatorView.center = CGPointMake(kXPointCenterIndicatorView, kYPointCenterIndicatorView);
        self.indicatorView.hidden = NO;
        [self.indicatorView startAnimating];
        self.countLabel.alpha = 0.0f;
        [self.indicatorView stopAnimating];
        self.signImgView.alpha = 1.0f;
    }else{
        [self.indicatorView stopAnimating];
        self.countLabel.alpha = 1.0f;
        self.signImgView.alpha = 0.0f;
    }
}

- (void)setSource:(MeSource *)source{
    _source = source;
    if (_source == nil) {
        self.editEnabled = NO;
    }else{
        self.editEnabled = YES;
    }
    
    if (_source) {
        self.nameLabel.text = self.source.name;
        self.signImgView.alpha = 0.0f;
        [self updateData];
        
        if (_needFlipAnimation) {
            _oldImage = self.channelImgView.image;
        }
        
        if (source.custom) {
            NSInteger index = arc4random() % 23;
            UIImage *randomImage = [UIImage imageNamed:[NSString stringWithFormat:@"ca_custom%d.png",index]];
            [self.nameLabel setFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
            [self.nameLabel setFont:FONT_TITLE(14.0f)];
            self.nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
            [self.channelImgView setImageWithURL:[NSURL URLWithString:source.icon] placeholderImage:randomImage];
        }else{
            [self.nameLabel setFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
            [self.nameLabel setFont:FONT_CONTENT(14.0f)];
            self.nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
            [self.channelImgView setImageWithURL:[NSURL URLWithString:source.icon] placeholderImage:self.channelImgView.image];
        }
    }
    
}

#pragma mark - getter
- (UIImageView *)channelImgView{
    if (!_channelImgView) {
        _channelImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-36)];
        _channelImgView.backgroundColor = COLOR_RGBA(31.0, 144.0, 202.0, 1.0);
        _channelImgView.contentMode = UIViewContentModeScaleAspectFill;
        _channelImgView.userInteractionEnabled = YES;
        _channelImgView.clipsToBounds = YES;
        
        [self.contentView addSubview:_channelImgView];
    }
    return _channelImgView;
}

- (UIImageView *)bottomImgView{
    if (!_bottomImgView) {
        _bottomImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 34)];
        _bottomImgView.backgroundColor = COLOR_RGB(40.0f, 40.0f, 40.0f);
        [self.contentView addSubview:_bottomImgView];
    }
    return _bottomImgView;
}

- (UILabel *)countLabel{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:self.bottomImgView.bounds];
        _countLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|
        UIViewAutoresizingFlexibleHeight;
        _countLabel.backgroundColor = [UIColor clearColor];
        _countLabel.textColor = COLOR_RGB(245.0f, 245.0f, 245.0f);
        _countLabel.font =  FONT_NUMBER(16.0f);
        _countLabel.textAlignment = ALIGNTYPE_CENTER;
        [self.bottomImgView addSubview:_countLabel];
    }
    return _countLabel;
}

- (UILabel *)nameLabel{      //sourceButton标题
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.bounds.size.width, self.bottomImgView.frame.size.height)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        _nameLabel.textColor = COLOR_RGB(255.0f, 255.0f, 255.0f);
        _nameLabel.font =  FONT_CONTENT(20.0f);
        _nameLabel.textAlignment = ALIGNTYPE_LEFT;
        [self.bottomImgView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UIImageView *)signImgView{
    if (!_signImgView) {
        _signImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bottomImgView.bounds.size.width-29, floorf((self.bottomImgView.bounds.size.height-14)/2), 19, 14)];
        _signImgView.backgroundColor = [UIColor clearColor];
        _signImgView.image = IMGFROMBUNDLE(@"bg_weibo_sign.png");
        [self.bottomImgView addSubview:_signImgView];
    }
    return _signImgView;
}

- (UIActivityIndicatorView *)indicatorView{
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _indicatorView.hidesWhenStopped = YES;
        _indicatorView.center = CGPointMake(kXPointCenterIndicatorView, kYPointCenterIndicatorView);
        [self.bottomImgView addSubview:_indicatorView];
    }
    return _indicatorView;
}

@end
