//
//  CommentCell.h
//  iTaoHD
//
//  Created by liunian on 13-12-9.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kCommentCellHeight 60
@class Comment;
@interface CommentCell : UITableViewCell
@property (nonatomic, retain) UIImageView    *line;
@property (nonatomic, retain) UIImageView    *avatar;
@property (nonatomic, retain) UILabel        *name;        //频道名
@property (nonatomic, retain) UILabel        *content;

- (void)updateWithComment:(Comment *)comment;

+ (CGFloat)heightWithComment:(Comment *)comment;
@end
