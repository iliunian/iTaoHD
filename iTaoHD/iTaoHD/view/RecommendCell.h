//
//  RecommendCell.h
//  Mooker
//
//  Created by Jikui Peng on 12-8-14.
//  Copyright (c) 2012年 banma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RecommendCellDelegate <NSObject>
@optional
- (void)pathAnimationDidFinishedWithImageView:(UIImageView *)imageView;

@end

@interface RecommendCell : UIView

@property (nonatomic, assign) id<RecommendCellDelegate>   delegate;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) CuzyTBKItem    *object;

- (void)startAnimation;
- (void)stopAnimation;
- (void)resizeImageView;
- (void)showRecommendContent:(NSString *)imageUrl;
@end
