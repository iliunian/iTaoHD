//
//  CustomKeyCell.m
//  iMiniTao
//
//  Created by liu nian on 13-8-22.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CustomKeyCell.h"


//频道背景颜色
#define COLOR_RGBA_SOURCE_BG  COLOR_RGBA(0, 174.0f, 238.0f, 0.6f)
#define COLOR_RGB_SOURCE_BG   COLOR_RGB(0, 174.0f, 238.0f)

#define COLOR_TABLECELL_SELECT_DAY      COLOR_RGB(234, 234, 234)
#define COLOR_TABLECELL_SELECT_NIGHT    COLOR_RGB(55, 55, 55)

@implementation CustomKeyCell

#define BUTTON_THRESHOLD 80



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backViewbackgroundColor = [UIColor whiteColor];
        self.revealDirection = RMSwipeTableViewCellRevealDirectionRight;
        self.animationType = RMSwipeTableViewCellAnimationTypeEaseOut;
        self.panElasticityStartingPoint = BUTTON_THRESHOLD;
    }
    return self;
}

-(UIButton*)deleteButton {
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setBackgroundImage:[UIImage imageNamed:@"DeleteButtonBackground"] forState:UIControlStateNormal];
        [_deleteButton setBackgroundImage:[UIImage imageNamed:@"DeleteButtonBackground"] forState:UIControlStateHighlighted];
        [_deleteButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
        [_deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteButton setFrame:CGRectMake(CGRectGetMaxX(self.frame) - BUTTON_THRESHOLD, 0, BUTTON_THRESHOLD, CGRectGetHeight(self.contentView.frame))];
        [_deleteButton addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
        [_deleteButton setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    }
    return _deleteButton;
}

-(void)didStartSwiping {
    [super didStartSwiping];
    [self.backView addSubview:self.deleteButton];
}

-(void)deleteAction {
    if ([self.demoDelegate respondsToSelector:@selector(swipeTableViewCellDidDelete:)]) {
        [self.demoDelegate swipeTableViewCellDidDelete:self];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)resetContentView {
    [UIView animateWithDuration:0.15f
                     animations:^{
                         self.contentView.frame = CGRectOffset(self.contentView.bounds, 0, 0);
                     }
                     completion:^(BOOL finished) {
                         self.shouldAnimateCellReset = YES;
                         [self cleanupBackView];
                     }];
}

-(void)cleanupBackView {
    [super cleanupBackView];
    [_deleteButton removeFromSuperview];
    _deleteButton = nil;
}

- (void)prepareForReuse{
    [super prepareForReuse];
    _srcImageView.image = nil;
    _subscribeImageView.image = nil;
    _meSource = nil;
}

- (void)enterToChannel
{
    if ([_demoDelegate respondsToSelector:@selector(enterChannelForCustomKeyCell:)]) {
        [_demoDelegate enterChannelForCustomKeyCell:self];
    }
}

- (void)handleSubscribedSource
{
    if ([_demoDelegate respondsToSelector:@selector(handleSubscribeForCustomKeyCell:)]) {
        [_demoDelegate handleSubscribeForCustomKeyCell:self];
    }
}

#pragma mark - getter
- (UIImageView *)srcImageView{
    if (!_srcImageView) {
        _srcImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(22, 14, 65, 62)];
        _srcImageView.backgroundColor = COLOR_RGB_SOURCE_BG;
        _srcImageView.contentMode = UIViewContentModeScaleAspectFill;
        _srcImageView.userInteractionEnabled = YES;
        _srcImageView.clipsToBounds = YES;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(enterToChannel)];
		[_srcImageView addGestureRecognizer:tapGesture];
        
        [self.contentView addSubview:_srcImageView];
    }
    return _srcImageView;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 18, 540-64, 20)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = COLOR_RGB(0, 0, 0);
        _nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UILabel *)summaryLabel{
    if (!_summaryLabel) {
        _summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 38, 540-64, 20)];
        _summaryLabel.backgroundColor = [UIColor clearColor];
        _summaryLabel.textColor = COLOR_RGB(136.0f, 136.0f, 136.0f);
        _summaryLabel.font = FONT_CONTENT(11.0f);
        [self addSubview:_summaryLabel];
    }
    return _summaryLabel;
}

- (UIImageView *)subscribeImageView{
    if (!_subscribeImageView) {
        UIImage  *img = IMGNAMED(@"bg_subscribe.png");
        _subscribeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(540-img.size.width-10, floorf((90-img.size.height)/2), img.size.width, img.size.height)];
        _subscribeImageView.backgroundColor = [UIColor clearColor];
        _subscribeImageView.contentMode = UIViewContentModeScaleAspectFill;
        _subscribeImageView.clipsToBounds = YES;
        _subscribeImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handleSubscribedSource)];
		[_subscribeImageView addGestureRecognizer:tapGesture];
        
        [self.contentView addSubview:_subscribeImageView];
    }
    return _subscribeImageView;
}
@end
