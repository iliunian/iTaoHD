//
//  ItemCell.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "LolayGridViewCell.h"
#import "StrikeThroughLabel.h"
#define kDefaultCellBottomBarHeight           30
@interface ItemCell : LolayGridViewCell

@property (nonatomic, retain) UIImageView        *containView;
@property (nonatomic, retain) UILabel        *nameLabel;
@property (nonatomic, retain) UILabel        *trading;
@property (nonatomic, retain) StrikeThroughLabel        *priceLabel;
@property (nonatomic, retain) UIImageView        *priceBgView;
@property (nonatomic, retain) UILabel        *promotionPriceLabel;
@property (nonatomic, retain) UIImageView    *srcImageView;
@property (nonatomic, retain) UIImageView    *postageImageView;
- (void)updateWithItem:(CuzyTBKItem *)item;
@end
