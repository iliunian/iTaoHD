//
//  ItemCell.m
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ItemCell.h"

#define COLOR_RGB_SOURCE_BG   COLOR_RGB(0, 174.0f, 238.0f)

@implementation ItemCell

- (id)initWithFrame:(CGRect)inRect reuseIdentifier:(NSString*)inReuseIdentifier
{
    self = [super initWithFrame:inRect reuseIdentifier:inReuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)prepareForReuse{
    [super prepareForReuse];
    [self.srcImageView setImage:nil];
    
    [self.priceLabel setText:nil];
    [_postageImageView setImage:nil];
}

- (void)updateWithItem:(CuzyTBKItem *)item{
    UIImage *image = [IMGNAMED(@"wall_bg_shadow.png") resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10,15, 10)];
    [self.containView setImage:image];
    [self.srcImageView setImageWithURL:[NSURL URLWithString:item.itemImageURLString] placeholderImage:IMGNAMED(@"emptyLarge.png") completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    [self.nameLabel setText:[NSString stringWithFormat:@"  %@",item.itemName]];
    [self.priceLabel setText:[NSString stringWithFormat:@"%@",item.itemPrice]];
    [self.promotionPriceLabel setText:[NSString stringWithFormat:@"￥%@",item.promotionPrice]];
//    [self.trading setText:[NSString stringWithFormat:@"售出:%@件",item.tradingVolumeInThirtyDays]];
//    [self.priceLabel setNeedsDisplay];
    if ([item.free_postage integerValue]) {
        [self.postageImageView setImage:IMGNAMED(@"NoPostage.png")];
    }
}

#pragma mark - getter
- (UIImageView *)containView{
    if (_containView == nil) {
        _containView = [[UIImageView alloc] initWithFrame:CGRectMake(4, 2, CGRectGetWidth(self.bounds) - 8, CGRectGetHeight(self.bounds) - 4)];
//        [_containView setClipsToBounds:YES];
        

        
        
//        [_containView.layer setBorderWidth:4];
//        [_containView.layer setBorderColor:[UIColor flatWhiteColor].CGColor];
        [self addSubview:_containView];
    }
    return _containView;
}
- (UIImageView *)srcImageView{
    if (!_srcImageView) {
        _srcImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, CGRectGetWidth(self.containView.bounds) - 6, CGRectGetHeight(self.containView.bounds) - 6 - kDefaultCellBottomBarHeight)];
        _srcImageView.backgroundColor = [UIColor clearColor];
        _srcImageView.contentMode = UIViewContentModeScaleAspectFill;
        _srcImageView.userInteractionEnabled = YES;
        _srcImageView.clipsToBounds = YES;
        [self.containView addSubview:_srcImageView];
    }
    return _srcImageView;
}
- (UIImageView *)postageImageView{
    if (!_postageImageView) {
        _postageImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.srcImageView.frame) - 32-5, CGRectGetHeight(self.srcImageView.frame) - 15 , 32, 32)];
        _postageImageView.backgroundColor = [UIColor clearColor];
        [_postageImageView setImage:IMGNAMED(@"NoPostage.png")];
        _postageImageView.contentMode = UIViewContentModeScaleAspectFill;
        _postageImageView.userInteractionEnabled = YES;
        _postageImageView.clipsToBounds = YES;
        [self.containView addSubview:_postageImageView];
    }
    return _postageImageView;
}


- (UIImageView *)priceBgView{
    if (!_priceBgView) {
        UIImage *image = [IMGNAMED(@"cell_bg_price.png") resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 20)];
        _priceBgView  = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 70, 27)];
        _priceBgView.backgroundColor = [UIColor clearColor];
        [_priceBgView setImage:image];
        [self.containView addSubview:_priceBgView];
    }
    return _priceBgView;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(4,
                                                               CGRectGetHeight(self.containView.bounds) - kDefaultCellBottomBarHeight,
                                                               CGRectGetWidth(self.containView.bounds) - 8,
                                                               kDefaultCellBottomBarHeight)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = [UIColor flatDarkBlackColor];
        _nameLabel.font = FONT_TITLE(16.0f);
        [_nameLabel setNumberOfLines:0];
        [self.containView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (StrikeThroughLabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[StrikeThroughLabel alloc] initWithFrame:CGRectMake(5, 5, 40, 25)];
        _priceLabel.backgroundColor = [UIColor clearColor];
        _priceLabel.textAlignment = ALIGN_LEFT;
        _priceLabel.textColor = [UIColor flatGrayColor];
        _priceLabel.font = FONT_CONTENT(10.0f);
        [_priceLabel setNumberOfLines:1];
        _priceLabel.strikeThroughEnabled = YES;
        [self.containView addSubview:_priceLabel];
    }
    return _priceLabel;
}
- (UILabel *)promotionPriceLabel{
    if (!_promotionPriceLabel) {
        _promotionPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, 90, 24)];
        _promotionPriceLabel.backgroundColor = [UIColor clearColor];
        _promotionPriceLabel.textColor = [UIColor flatWhiteColor];
        _promotionPriceLabel.font = FONT_NUMBER(18.0f);
        [_promotionPriceLabel setNumberOfLines:1];
        [self.priceBgView addSubview:_promotionPriceLabel];
    }
    return _promotionPriceLabel;
}
- (UILabel *)trading{
    if (!_trading) {
        _trading = [[UILabel alloc] initWithFrame:CGRectMake(80, 163, 70, 25)];
        _trading.backgroundColor = [UIColor clearColor];
        _trading.textColor = [UIColor flatRedColor];
        _trading.font = FONT_CONTENT(12.0f);
        [_trading setTextAlignment:ALIGN_RIGHT];
        [_trading setNumberOfLines:1];
        [self.containView addSubview:_trading];
    }
    return _trading;
}
@end
