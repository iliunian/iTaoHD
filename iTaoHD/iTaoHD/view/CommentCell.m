//
//  CommentCell.m
//  iTaoHD
//
//  Created by liunian on 13-12-9.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CommentCell.h"
#import "Comment.h"

@implementation CommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{

}

- (void)updateWithComment:(Comment *)comment{

    [self.line setFrame:CGRectMake(10, 0, 375 - 20, 1)];
    
    [self.avatar setImageWithURL:[NSURL URLWithString:comment.avatar_url] placeholderImage:IMGNAMED(@"bg_weibo_user.png")];
    [self.name setText:comment.name];
    [self.content setText:comment.message];
    [self.name setFrame:CGRectMake(70, 5, 200, 20)];
    CGSize size = [comment.message sizeWithFont:self.content.font constrainedToSize:CGSizeMake(375- 80, 1000) lineBreakMode:NSUILineBreakModeWordWrap];
    if (size.height < 40) {
        size.height = 40;
    }
    
    [self.content setFrame:CGRectMake(70, 30, 375- 80, size.height)];

}

+ (CGFloat)heightWithComment:(Comment *)comment{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, 375- 80, 35)];
    label.font =  FONT_CONTENT(14.0f);
    label.numberOfLines = 0;
    label.textAlignment = ALIGNTYPE_LEFT;
    
    CGSize size = [comment.message sizeWithFont:label.font constrainedToSize:CGSizeMake(375- 80, 1000) lineBreakMode:NSUILineBreakModeWordWrap];
    if (size.height < 40) {
        size.height = 40;
    }
    return size.height + 40;
}
- (UIImageView *)line{
    if (!_line) {
        _line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 1)];
        _line.backgroundColor = [UIColor clearColor];
        [_line setImage:IMGNAMED(@"bg_pointline")];
        _line.contentMode = UIViewContentModeScaleAspectFill;
        _line.userInteractionEnabled = YES;
        _line.clipsToBounds = YES;
        [self.contentView addSubview:_line];
    }
    return _line;
}
- (UIImageView *)avatar{
    if (!_avatar) {
        _avatar = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
        _avatar.backgroundColor = [UIColor clearColor];
        _avatar.contentMode = UIViewContentModeScaleAspectFill;
        _avatar.userInteractionEnabled = YES;
        _avatar.clipsToBounds = YES;
        [self.contentView addSubview:_avatar];
    }
    return _avatar;
}

- (UILabel *)name{
    if (!_name) {
        _name = [[UILabel alloc] initWithFrame:CGRectMake(70, CGRectGetMinY(self.avatar.frame), 200, 20)];

        _name.backgroundColor = [UIColor clearColor];
        _name.textColor = [UIColor flatBlackColor];
        _name.font =  FONT_TITLE(16.0f);
        _name.textAlignment = ALIGN_LEFT;
        [self.contentView addSubview:_name];
    }
    return _name;
}

- (UILabel *)content{      //sourceButton标题
    if (!_content) {
        _content = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, self.bounds.size.width - 80, 35)];
        _content.backgroundColor = [UIColor clearColor];
        //        _nameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        _content.textColor = [UIColor flatBlackColor];
        _content.font =  FONT_CONTENT(14.0f);
        _content.numberOfLines = 0;
        _content.textAlignment = ALIGNTYPE_LEFT;
        [self.contentView addSubview:_content];
    }
    return _content;
}
@end
