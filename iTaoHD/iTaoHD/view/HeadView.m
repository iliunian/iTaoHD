//
//  HeadView.m
//  suitDress
//
//  Created by liunian on 13-10-16.
//  Copyright (c) 2013年 liu nian. All rights reserved.
//

#import "HeadView.h"

@implementation HeadView

- (id)initWithFrame:(CGRect)frame withItem:(CuzyTBKItem *)item
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initWithPictures:item.picturesArray];
    }
    return self;
}

- (void)initWithPictures:(NSArray*)pictures{
    for (int i = 0; i < [pictures count]; i++) {
        CGRect frame = CGRectMake(i*CGRectGetWidth(self.scrollView.frame), 0, CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.scrollView.frame));
        UIImageView  *picture = [[UIImageView alloc] initWithFrame:frame];
        [picture setContentMode:UIViewContentModeScaleAspectFill];
        [picture setClipsToBounds:YES];
        [picture setBackgroundColor:[UIColor clearColor]];
        [picture setImageWithURL:[NSURL URLWithString:[pictures objectAtIndex:i]] placeholderImage:IMGNAMED(@"")];
        [self.scrollView addSubview:picture];
    }
    [self.scrollView setContentSize:CGSizeMake(pictures.count * CGRectGetWidth(self.scrollView.frame) , CGRectGetHeight(self.scrollView.frame))];
    if (pictures.count > 1) {
        [self.pageControl setNumberOfPages:pictures.count];
        [self.pageControl setCurrentPage:0];
    }
    
}

- (void)scrollRectToVisibleByPage:(int)page{
    self.currentPage = page;
}
- (void)pageValueChange:(id)sender{
    UIPageControl  *contol = (UIPageControl *)sender;
    [self scrollRectToVisibleByPage:contol.currentPage];
}
#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger pageIndex = scrollView.contentOffset.x/CGRectGetWidth(scrollView.frame);
    [self scrollRectToVisibleByPage:pageIndex];
}

- (void)setCurrentPage:(NSInteger)currentPage{
    [self.pageControl setCurrentPage:currentPage];
}
#pragma mark getter
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds))];
        _scrollView.delegate = self;
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:_scrollView];
    }
    return _scrollView;
}

- (MKPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[MKPageControl alloc] initWithFrame:CGRectMake(140, self.bounds.size.height-20, 40, 22)];
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.isBigDot = YES;
        [_pageControl setImagePageStateNormal:IMGFROMBUNDLE(@"bg_page_normal.png")];
        [_pageControl setImagePageStateHightlighted:IMGFROMBUNDLE(@"bg_page_select.png")];
        [self addSubview:_pageControl];
        [_pageControl addTarget:self
                         action:@selector(pageValueChange:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

@end
