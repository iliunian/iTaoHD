//
//  MainViewController.h
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ViewController.h"
#import "SourceView.h"

@interface MainViewController : ViewController
@property (nonatomic, assign) BOOL slideEnabled;
@property (nonatomic, assign) BOOL mainViewShown;
@property (nonatomic, retain) SourceView *sourceView;
@end
