//
//  DataEnvironment.m
//  FlipBoard
//
//  Created by Kevin Huang on 9/15/10.
//  Copyright 2010 iTotem. All rights reserved.
//

#import "DataEnvironment.h"
#import "MainViewController.h"

@implementation DataEnvironment
static DataEnvironment *sharedInst = nil;

@synthesize mainViewController = _mainViewController;
@synthesize navController = _navController;
@synthesize numOfDetailView = _numOfDetailView;
@synthesize isMainViewPresentingController = _isMainViewPresentingController;
@synthesize weiboViewController = _weiboViewController;

+ (id)sharedDataEnvironment 
{
	@synchronized( self ) 
	{
		if ( sharedInst == nil ) 
		{
            sharedInst = [[self alloc] init];
		}
	}
	return sharedInst;
}

- (id)init {
	if ( sharedInst != nil )
    {
        return sharedInst;
	}
    
    self = [super init];
	if (self)
    {
		sharedInst = self;
		[self initData];
	}
	return self;
}

- (void)initData 
{
    _numOfDetailView = 0;
	self.mainViewController = nil;
    self.navController = nil;
    self.weiboViewController = nil;
    self.isMainViewPresentingController = NO;
}
- (MKNavigationController *)currentNavigation
{
    if (self.collectionNav != nil)
    {
        return self.collectionNav;
    }
    else
    {
        return self.navController;
    }
}
- (UIViewController *)presentingController
{
    UIViewController *viewController = nil;
    UIViewController *currentController = self.navController;
    while (currentController != nil){
        viewController = currentController;
        currentController = currentController.modalViewController;
    }
    return viewController;
}
@end


