//
//  ItemViewController.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ViewController.h"

@interface ItemViewController : ViewController
@property (nonatomic, retain) UIImage   *shareImage;
- (id)initWithMeItem:(CuzyTBKItem *)item;
@end
