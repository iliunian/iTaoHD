//
//  CommentViewController.h
//  iTaoHD
//
//  Created by liunian on 13-12-9.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController
- (id)initWithCuzyItem:(CuzyTBKItem *)item image:(UIImage *)image;
@end

@interface Author : NSObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *avatar;
@property (nonatomic, retain) NSString *url;
@end