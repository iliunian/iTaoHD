//
//  DesktopController.m
//  suitDress
//
//  Created by liunian on 13-10-17.
//  Copyright (c) 2013年 liu nian. All rights reserved.
//

#import "DesktopController.h"
#import "SlideImageView.h"
#import "ApplicationManager.h"





#define BROWSER_TITLE_LBL_TAG 12731
#define BROWSER_DESCRIP_LBL_TAG 178273
#define BROWSER_LIKE_BTN_TAG 12821

@interface DesktopController ()<SlideImageViewDelegate,UIAlertViewDelegate>{
    NSInteger   _clickIndex;
}
@property (nonatomic, retain) UIImageView          *navImageView;
@property (nonatomic, retain) SlideImageView *slideImageView;
@end

@implementation DesktopController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_RGB(248, 248, 248);
    _clickIndex = 0;
    self.view.backgroundColor = COLOR_RGB(248, 248, 248);
    
    [self.view addSubview:self.navImageView];
    for(int i=0; i<14; i++)
    {
        NSString* imageName = [NSString stringWithFormat:@"bg_source_%d.jpg",i];
        UIImage* image = [UIImage imageNamed:imageName];
        [self.slideImageView addImage:image];
    }
    [self.slideImageView setImageShadowsWtihDirectionX:2 Y:2 Alpha:0.7];
    [self.slideImageView reLoadUIview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark SlideImageViewDelegate
- (void)SlideImageViewDidClickWithIndex:(int)index
{
    NSString* indexStr = [[NSString alloc]initWithFormat:@"点击了第%d张",index];
    BMLog(@"%@",indexStr);
    _clickIndex = index;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定设置该图片为桌面背景么?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert setTag:10013];
    [alert show];
}

- (void)SlideImageViewDidEndScorllWithIndex:(int)index
{
    NSString* indexStr = [[NSString alloc]initWithFormat:@"当前为第%d张",index];
}
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        return;
    }
     NSString* imageName = [NSString stringWithFormat:@"bg_source_%d.jpg",_clickIndex];
    [[ApplicationManager sharedManager] setBackgroundImage:imageName];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"桌面背景设置成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alert show];
}
#pragma mark getter
- (SlideImageView*)slideImageView{
    if (nil == _slideImageView) {
        CGRect rect;
        rect = CGRectMake(20,
                          CGRectGetHeight(self.navImageView.frame)+ 50,
                          CGRectGetWidth(self.navImageView.frame) - 80,
                          620-CGRectGetHeight(self.navImageView.frame) - 100);
        
        _slideImageView = [[SlideImageView alloc]initWithFrame:rect ZMarginValue:5 XMarginValue:10 AngleValue:0.3 Alpha:1000];
        _slideImageView.borderColor = [UIColor flatWhiteColor];
        _slideImageView.delegate = self;
        [self.view addSubview:_slideImageView];
    }
    return _slideImageView;
}
- (UIImageView *)navImageView{
    if (!_navImageView) {
        UIImage  *img = IMGNAMED(@"bg_nav_modal.png");
        img = [img stretchableImageWithLeftCapWidth:floor(img.size.width/2) topCapHeight:0];
        _navImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 540, 63)];
        _navImageView.backgroundColor = [UIColor clearColor];
        _navImageView.userInteractionEnabled = YES;
        _navImageView.image = img;
        
        UILabel  *title = [[UILabel alloc] initWithFrame:CGRectMake((_navImageView.bounds.size.width-200)/2,
                                                                    0, 200, _navImageView.bounds.size.height)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = COLOR_RGB(0, 0, 0);
        title.textAlignment = ALIGNTYPE_CENTER;
        title.font = FONT_TITLE(26.0f);
        title.text = @"频道订阅";
        [_navImageView addSubview:title];
        
        UIImage *imgBtn = IMGNAMED(@"bg_nav_button_small.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, (_navImageView.bounds.size.height-imgBtn.size.height)/2,
                                imgBtn.size.width, imgBtn.size.height);
        [back setBackgroundImage:imgBtn forState:UIControlStateNormal];
        [back setTitleColor:COLOR_RGB(0,0,0) forState:UIControlStateNormal];
        [back setTitle:@"关闭" forState:UIControlStateNormal];
        back.titleLabel.font = FONT_CONTENT(16.0f);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_navImageView addSubview:back];
        
    }
    return _navImageView;
}
@end
