//
//  PageViewController.h
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlipView.h"

@interface PageViewController : UIViewController
@property (nonatomic, retain) UIView        *containerView;
@property (nonatomic, retain) FlipView      *flipView;
@property (nonatomic, assign) NSInteger     numberOfPages;
@end
