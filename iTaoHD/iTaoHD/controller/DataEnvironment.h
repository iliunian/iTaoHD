//
//  DataEnvironment.h
//  Mooker
//
//  Created by Kevin Huang on 9/15/10.
//  Copyright 2010 iTotem. All rights reserved.
//
#import "MKNavigationController.h"

@class MainViewController;
@interface DataEnvironment : NSObject {

}

+ (DataEnvironment *)sharedDataEnvironment;

@property(nonatomic,retain) MainViewController *mainViewController;
@property(nonatomic,retain) MKNavigationController *navController;
@property(nonatomic,retain) MKNavigationController *collectionNav;
@property(nonatomic,assign) NSInteger numOfDetailView;
@property(nonatomic,assign) BOOL  isMainViewPresentingController;

@property(nonatomic,assign) UIViewController *weiboViewController;
- (void)initData;
- (UIViewController *)presentingController;
- (MKNavigationController *)currentNavigation;
@end


