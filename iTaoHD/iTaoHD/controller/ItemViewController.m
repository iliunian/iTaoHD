//
//  ItemViewController.m
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ItemViewController.h"
#import "WebViewController.h"
#import "FavoritManager.h"
#import "UMSocial.h"
#import "SNSManager.h"
#import "JSON.h"
#import "HeadView.h"
#import "SDImageCache.h"
#import <QuartzCore/QuartzCore.h>
#import "CommentRequest.h"
#import "WeiboViewController.h"
#import "CommentRequest.h"
#import "CommentViewController.h"
#import "CommentCell.h"
#import "Comment.h"


#define BROWSER_TITLE_LBL_TAG 12731
#define BROWSER_DESCRIP_LBL_TAG 178273
#define BROWSER_LIKE_BTN_TAG 12821

#define kDefaultMenuViewHeight            44
#define kDefaultTopBarHeight              39
#define kDefaultColorHeadHeight           3

#define kLeftDivWidth           375

@interface ItemViewController ()<UIWebViewDelegate,SNSManagerDelegate,UMSocialUIDelegate,UITableViewDataSource,UITableViewDelegate,ReqDelegate>{
}
@property (nonatomic, strong) CuzyTBKItem *item;
@property (nonatomic, strong) UIImageView          *bottomImageView;
@property (nonatomic, strong) UIButton      *favBtn;
@property (nonatomic, strong) UIButton      *forwardBtn;
@property (nonatomic, strong) UIButton      *commentBtn;
@property (nonatomic, strong) NSString      *shortenUrl;

@property (nonatomic, strong) HeadView *headView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray    *datasource;
@property (nonatomic, strong) UIView    *headerView;
@property (nonatomic, strong) UILabel   *nameLabel;
@property (nonatomic, strong) UILabel   *priceLabel;
@property (nonatomic, strong) UILabel   *promotionPriceLabel;
@property (nonatomic, strong) UIImageView *shadowImageView;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIView    *menuView;

@property (nonatomic, strong) UIImageView *colorHeadImage;
@property (nonatomic, strong) UIImageView *shadowHeadImage;
@property (nonatomic, strong) UIButton  *backwardButton;
@property (nonatomic, strong) UIButton  *forwardButton;
@property (nonatomic, strong) UIButton  *refreshButton;
@property (nonatomic, strong) CommentRequest *commentReq;
@end

@implementation ItemViewController

- (void)dealloc{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [[SNSManager sharedManager] cancelRequestForDelegate:self];
    self.bottomImageView = nil;
    self.item = nil;
    self.favBtn = nil;
    self.forwardBtn = nil;
    self.shortenUrl = nil;
    self.headerView = nil;
    self.nameLabel = nil;
    self.priceLabel = nil;
    self.promotionPriceLabel = nil;
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.tableView=nil;
    self.shadowImageView = nil;
    self.URL = nil;
    self.webView.delegate = nil;
    self.webView = nil;
    self.menuView = nil;
    self.colorHeadImage = nil;
    self.backwardButton = nil;
    self.forwardButton = nil;
    self.refreshButton = nil;
}

- (id)initWithMeItem:(CuzyTBKItem *)item
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.item = item;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 70000
    
#else
    float systemName = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(systemName >= 7.0)
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
#endif
//    self.view.backgroundColor = [UIColor blackColor];
    [self colorHeadImage];
    NSString *clickURL = [@"http://" stringByAppendingFormat:@"%@", self.item.itemClickURLString];
    self.URL = [NSURL URLWithString:clickURL];
    [[SNSManager sharedManager] covertToShotURL:clickURL withDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.bottomImageView = nil;
    self.item = nil;
    self.favBtn = nil;
    self.forwardBtn = nil;
    self.shortenUrl = nil;
    self.headerView = nil;
    self.nameLabel = nil;
    self.priceLabel = nil;
    self.promotionPriceLabel = nil;
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.tableView=nil;
    self.shadowImageView = nil;
    self.URL = nil;
    self.webView.delegate = nil;
    self.webView = nil;
    self.menuView = nil;
    self.colorHeadImage = nil;
    self.backwardButton = nil;
    self.forwardButton = nil;
    self.refreshButton = nil;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self initHeadView];
    
    [self.tableView reloadData];
    
    [self updateUI];
    
    [self webView];
    [self shadowImageView];
    [self menuView];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.URL]];
    
    [self shadowHeadImage];
    [self bottomImageView];
    [self reloadComments];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          self.item.itemName, @"itemName",self.item.itemClickURLString, @"itemClickURLString", nil];
    [MobClick event:@"showProduct" attributes:dict];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self orientationChanged:[UIApplication sharedApplication].statusBarOrientation];
}
#pragma mark -
#pragma mark  handle orientation change
- (void)orientationChanged:(UIInterfaceOrientation)currentOrientation
{
    
}

- (void)reloadComments{
    [self.commentReq getCommentWithItemID:[NSString stringWithFormat:@"%d",[self.item.itemID intValue]] withDelegate:self];
}

- (void)updateUI{
    if ([[FavoritManager sharedManager] isFavorit:self.item]) {
        [self.favBtn  setBackgroundImage:IMGNAMED(@"bt_unfocus.png") forState:UIControlStateNormal];
        [self.favBtn  setBackgroundImage:IMGNAMED(@"bt_focus.png") forState:UIControlStateHighlighted];
    }else{
        [self.favBtn  setBackgroundImage:IMGNAMED(@"bt_focus.png") forState:UIControlStateNormal];
        [self.favBtn  setBackgroundImage:IMGNAMED(@"bt_unfocus.png") forState:UIControlStateHighlighted];
    }
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initHeadView{
    if (self.item.picturesArray) {
        _headView = [[HeadView alloc] initWithFrame:CGRectMake(0, 0, kLeftDivWidth, kLeftDivWidth) withItem:self.item];
    }
}

- (void)favBtnClick:(id)sender{
    
    if ([[FavoritManager sharedManager] isFavorit:self.item]) {
        [[FavoritManager sharedManager] removeItem:self.item];
    }else{
        [[FavoritManager sharedManager] favoritItem:self.item];
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              self.item.itemName, @"itemName",self.item.itemClickURLString, @"itemClickURLString", nil];
        [MobClick event:@"collectProduct" attributes:dict];
    }
    [self updateUI];
}
- (void)forwardBtnClick{
    if (self.item == nil) {
        return;
    }
    WeiboViewController *viewController = [[WeiboViewController alloc]   initWithCuzyItem:self.item image:self.shareImage infoString:self.shortenUrl];
    MKNavigationController  *nav = [[MKNavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBarHidden = YES;
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [[[DataEnvironment sharedDataEnvironment] presentingController] presentModalViewController:nav animated:YES];
    RELEASE_SAFELY(viewController);
    RELEASE_SAFELY(nav);
    return;
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:kUmengAppKey
                                      shareText:self.item.itemName
                                     shareImage:self.shareImage
                                shareToSnsNames:nil
                                       delegate:self];
    
}

- (void)commentBtnClick{
    if (self.item == nil) {
        return;
    }
    CommentViewController *viewController = [[CommentViewController alloc]   initWithCuzyItem:self.item image:self.shareImage];
    MKNavigationController  *nav = [[MKNavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBarHidden = YES;
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [[[DataEnvironment sharedDataEnvironment] presentingController] presentModalViewController:nav animated:YES];
    RELEASE_SAFELY(viewController);
    RELEASE_SAFELY(nav);
}

#pragma mark ReqDelegate
- (void)finishErrorReq:(id)Req errorCode:(int) errorCode message:(NSString *)message{}

- (void)finishSourceReq:(id)Req itemID:(NSString *)itemid comments:(NSArray *)comments{
    if (comments && comments.count > 0) {
        [self.datasource addObjectsFromArray:comments];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}
#pragma mark UMSocialUIDelegate
-(void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData{
    socialData.shareText = [NSString stringWithFormat:@"%@[%@]..",socialData.shareText,self.shortenUrl];
    UMSocialUrlResource *urlRes = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:self.item.itemImageURLString];
    socialData.urlResource = urlRes;
    
    if (platformName == UMShareToWechatSession) {
        [UMSocialData defaultData].extConfig.title = self.item.itemName;   //分享标题，分享到朋友圈只能显示标题内容
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeOther; //设置为网页、音乐等其他类型
        WXWebpageObject *webObject = [WXWebpageObject object];    //初始化微信网页对象
        webObject.webpageUrl = self.shortenUrl; //设置网页的url地址
        [UMSocialData defaultData].extConfig.wxMediaObject = webObject; //设置网页对象
    }
}
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          self.item.itemName, @"itemName",self.item.itemClickURLString, @"itemClickURLString", nil];
    [MobClick event:@"ShareProduct" attributes:dict];
}
#pragma mark SNSManagerDelegate
- (void)snsManagerShortenUrlDidFailFor:(OFTypeE)type{
    
}
- (void)snsManagerShortenUrlDidSuccessFor:(OFTypeE)type withResult:(id)result{
    NSDictionary *jsonDict = result;
    if (isDictWithCountMoreThan0(jsonDict)) {
        NSArray *urls = [jsonDict objectForKey:@"urls"];
        NSDictionary *resultDic = [urls lastObject];
        if ([resultDic objectForKey:@"url_short"] && [[resultDic objectForKey:@"result"] boolValue]) {
            self.shortenUrl = [resultDic objectForKey:@"url_short"];
            BMLog(@"self.shortenUrl%@",self.shortenUrl);
        }
    }
    
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        Comment *comment = [self.datasource objectAtIndex:indexPath.row];
        return [CommentCell heightWithComment:comment];
    }
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 0;
    }
    return 54;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return nil;
    }
    CGFloat promotionPrice =[self.item.promotionPrice floatValue];
    CGFloat itemPrice =[self.item.itemPrice floatValue];
    [self.nameLabel setText:self.item.itemName];
    if ([self.item.itemPrice isEqualToString:self.item.promotionPrice]) {
        
        [self.promotionPriceLabel setText:[NSString stringWithFormat:@"￥%.0f",promotionPrice]];
    }else{
        [self.promotionPriceLabel setText:[NSString stringWithFormat:@"￥%.0f",promotionPrice]];
        [self.priceLabel setText:[NSString stringWithFormat:@"节省%.0f元",itemPrice - promotionPrice]];
    }
    return self.headerView;
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return self.datasource.count;
    }
    return 3;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        static NSString *identifier = @"comment";
        CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40)];
            [bg setBackgroundColor:[UIColor whiteColor]];
            [bg setImage:IMGNAMED(@"listCellBackground.png")];
            [bg setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.9]];
            [cell.contentView addSubview:bg];
        }
        Comment *comment = [self.datasource objectAtIndex:indexPath.row];
        [cell updateWithComment:comment];
        return cell;
    }
    static NSString *identifier = @"item";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40)];
        [bg setBackgroundColor:[UIColor whiteColor]];
        [bg setImage:IMGNAMED(@"listCellBackground.png")];
        [bg setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.9]];
        [cell.contentView addSubview:bg];
    }
    
    
    
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
    switch (indexPath.row) {
        case 0:
        {
        NSString *str = [self.item.free_postage integerValue] == 1 ? @"包邮": @"不包邮";
        [cell.textLabel setText:@"是否包邮"];
        [cell.detailTextLabel setText:str];
        }
            break;
        case 1:
        {
            NSString *str = @"未知";
            if ([self.item.item_type integerValue] == 1) {
                str = @"淘宝";
            }else if ([self.item.item_type integerValue] == 2){
                str = @"天猫";
            }
            
        [cell.textLabel setText:@"所属商城"];
        [cell.detailTextLabel setText:str];
        }
            break;
        case 2:
        {
        NSString *str = [NSString stringWithFormat:@"%@件",self.item.tradingVolumeInThirtyDays];
        [cell.textLabel setText:@"30天售出"];
        [cell.detailTextLabel setText:str];
        
        }
            break;
        default:
            break;
    }
    
    return cell;
}
- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(self.view.bounds) - kDefaultBottomBarHeight -_kSpaceHeight,
                                                                         CGRectGetWidth(self.view.bounds),
                                                                         kDefaultBottomBarHeight)];
        _bottomImageView.backgroundColor = COLOR_RGB_BOTTOM;
        _bottomImageView.userInteractionEnabled = YES;
//        _bottomImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth;
        
        UIImage *image = [IMGNAMED(@"bg_navback.png") resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
        [_bottomImageView setImage:image];
        
        UIImage *imgBack = IMGNAMED(@"btn_back.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, floorf((_bottomImageView.bounds.size.height-imgBack.size.height)/2), imgBack.size.width, imgBack.size.height);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_bottomImageView addSubview:back];
        
        [_bottomImageView addSubview:self.favBtn];
        [_bottomImageView addSubview:self.commentBtn];
        [_bottomImageView addSubview:self.forwardBtn];
        [back setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [back setBackgroundImage:IMGNAMED(@"btn_back_light.png") forState:UIControlStateHighlighted];
        [self.view addSubview:_bottomImageView];
    }
    return _bottomImageView;
}

- (UIButton *)favBtn{
    if (!_favBtn) {
        UIImage *imgTop = IMGNAMED(@"bt_unfocus.png");
        _favBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _favBtn.exclusiveTouch = YES;
        _favBtn.frame = CGRectMake(kLeftDivWidth- 3 *(imgTop.size.width+10), floorf((kDefaultBottomBarHeight-imgTop.size.height)/2), imgTop.size.width, imgTop.size.height);
        [_favBtn addTarget:self action:@selector(favBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_favBtn setBackgroundImage:IMGNAMED(@"bt_focus.png") forState:UIControlStateNormal];
        [_favBtn setBackgroundImage:IMGNAMED(@"bt_unfocus.png") forState:UIControlStateHighlighted];
    }
    return _favBtn;
}

- (UIButton *)commentBtn{
    if (!_commentBtn) {
        
        UIImage *imgTop = IMGNAMED(@"btn_new_comments.png");
        _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentBtn.exclusiveTouch = YES;
        _commentBtn.frame = CGRectMake(kLeftDivWidth-2*( imgTop.size.width+10), floorf((kDefaultBottomBarHeight-imgTop.size.height)/2), imgTop.size.width, imgTop.size.height);
        [_commentBtn addTarget:self action:@selector(commentBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_commentBtn setBackgroundImage:IMGNAMED(@"btn_new_comments.png") forState:UIControlStateNormal];
        [_commentBtn setBackgroundImage:IMGNAMED(@"btn_new_comments_light.png") forState:UIControlStateHighlighted];
    }
    return _commentBtn;
}

- (UIButton *)forwardBtn{
    if (!_forwardBtn) {
        
        UIImage *imgTop = IMGNAMED(@"btn_forward.png");
        _forwardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _forwardBtn.exclusiveTouch = YES;
        _forwardBtn.frame = CGRectMake(kLeftDivWidth-imgTop.size.width-10, floorf((kDefaultBottomBarHeight-imgTop.size.height)/2), imgTop.size.width, imgTop.size.height);
        [_forwardBtn addTarget:self action:@selector(forwardBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_forwardBtn setBackgroundImage:IMGNAMED(@"btn_forward.png") forState:UIControlStateNormal];
        [_forwardBtn setBackgroundImage:IMGNAMED(@"btn_forward_light.png") forState:UIControlStateHighlighted];
    }
    return _forwardBtn;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                   CGRectGetMaxY(self.colorHeadImage.frame),
                                                                   kLeftDivWidth,
                                                                   CGRectGetHeight(self.view.bounds) - kDefaultBottomBarHeight - kDefaultColorHeadHeight -_kSpaceHeight)
                                                  style:UITableViewStylePlain];
        
        [_tableView setBackgroundColor:[UIColor flatWhiteColor]];
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setTableHeaderView:self.headView];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 54)];
        [_headerView setBackgroundColor:[UIColor whiteColor]];
        UIImageView *accView = [[UIImageView alloc] initWithFrame:_headerView.frame];
        [accView setBackgroundColor:[UIColor whiteColor]];
        [accView setImage:IMGNAMED(@"note_bg_onesimilar.png")];
        [_headerView addSubview:accView];
        [_headerView addSubview:self.nameLabel];
        [_headerView addSubview:self.promotionPriceLabel];
    }
    return _headerView;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 10, 230, 40)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = FONT_CONTENT(14.0f);
        [_nameLabel setNumberOfLines:0];
        
    }
    return _nameLabel;
}

- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(280, CGRectGetMinY(self.promotionPriceLabel.frame), 30, CGRectGetHeight(self.promotionPriceLabel.frame)) ];
        _priceLabel.backgroundColor = [UIColor clearColor];
        _priceLabel.textAlignment = ALIGN_CENTER;
        _priceLabel.textColor = [UIColor flatGrayColor];
        _priceLabel.font = FONT_CONTENT(14.0f);
        [_priceLabel setNumberOfLines:1];
    }
    return _priceLabel;
}
- (UILabel *)promotionPriceLabel{
    if (!_promotionPriceLabel) {
        _promotionPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 80, 40)];
        _promotionPriceLabel.backgroundColor = [UIColor clearColor];
        _promotionPriceLabel.textColor = [UIColor darkTextColor];
        _promotionPriceLabel.textAlignment = ALIGN_LEFT;
        _promotionPriceLabel.font = FONT_CONTENT(22.0f);
        [_promotionPriceLabel setNumberOfLines:1];
    }
    return _promotionPriceLabel;
}
- (UIImageView *)shadowImageView{
    if (!_shadowImageView) {
        UIImage *image = IMGNAMED(@"bg_recommend_shadow_1.png");
        _shadowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kLeftDivWidth,
                                                                         (CGRectGetHeight(self.view.frame) - image.size.height)/2,
                                                                         image.size.width,
                                                                         image.size.height)];
        _shadowImageView.image = image;
        _shadowImageView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_shadowImageView];
    }
    return _shadowImageView;
    
}
- (UIImageView *)shadowHeadImage{
    if (!_shadowHeadImage) {
        UIImage *image = IMGNAMED(@"bg_recommend_shadow.png");
        _shadowHeadImage = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame) - image.size.width)/2,
                                                                         CGRectGetMaxY(self.colorHeadImage.frame),
                                                                         image.size.width,
                                                                         image.size.height)];
        _shadowHeadImage.image = image;
        _shadowHeadImage.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_shadowHeadImage];
    }
    return _shadowHeadImage;
    
}

- (NSMutableArray *)datasource{
    if (!_datasource) {
        _datasource = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return _datasource;
}

- (CommentRequest *)commentReq{
    if (!_commentReq) {
        _commentReq = [[CommentRequest alloc] init];
    }
    return _commentReq;
}

- (UIImageView *)colorHeadImage
{
    if (!_colorHeadImage)
        {
        _colorHeadImage = [[UIImageView alloc] initWithImage:IMGNAMED(@"bg_colorhead.png")];
        _colorHeadImage.frame = CGRectMake(0, 0, 1024, kDefaultColorHeadHeight);
        [self.view addSubview:_colorHeadImage];
        }
    return _colorHeadImage;
}

- (UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(kLeftDivWidth,
                                                               CGRectGetMinY(self.tableView.frame),
                                                               CGRectGetWidth(self.view.frame) - kLeftDivWidth,
                                                               CGRectGetHeight(self.view.bounds) - kDefaultBottomBarHeight -_kSpaceHeight)];
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _webView.scalesPageToFit = YES;
        _webView.delegate = self;
        [_webView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_webView];
    }
    return _webView;
}

- (UIView *)menuView {
    if (!_menuView) {
        _menuView = [[UIView alloc] initWithFrame:CGRectMake(kLeftDivWidth,
                                                             0,
                                                             self.bottomImageView.frame.size.width - kLeftDivWidth,
                                                             kDefaultBottomBarHeight)];
        _menuView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        
        _backwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backwardButton.exclusiveTouch = YES;
        UIImage *image = IMGNAMED(@"btn_web_backward.png");
        _backwardButton.frame = CGRectMake(floor(_menuView.bounds.size.width/2)-40-image.size.width,
                                           floor((_menuView.bounds.size.height-image.size.height)/2),
                                           image.size.width,
                                           image.size.height);
        [_backwardButton addTarget:self action:@selector(backwardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _backwardButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        [_menuView addSubview:_backwardButton];
        
        _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _forwardButton.exclusiveTouch = YES;
        image = IMGNAMED(@"btn_web_forward.png");
        _forwardButton.frame = CGRectMake(floor(_menuView.bounds.size.width/2)+10,
                                          floor((_menuView.bounds.size.height-image.size.height)/2),
                                          image.size.width,
                                          image.size.height);
        [_forwardButton addTarget:self action:@selector(forwardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _forwardButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        [_menuView addSubview:_forwardButton];
        
        _refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _refreshButton.exclusiveTouch = YES;
        image = IMGNAMED(@"btn_refresh.png");
        _refreshButton.frame = CGRectMake(_menuView.bounds.size.width- 2 * (10+image.size.width),
                                          floor((_menuView.bounds.size.height-image.size.height)/2),
                                          image.size.width,
                                          image.size.height);
        [_refreshButton addTarget:self action:@selector(refreshButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _refreshButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
        [_menuView addSubview:_refreshButton];
        
        _menuView.backgroundColor = [UIColor clearColor];
        [_backwardButton setBackgroundImage:IMGNAMED(@"btn_web_backward.png") forState:UIControlStateNormal];
        [_backwardButton setBackgroundImage:IMGNAMED(@"btn_web_backward_light.png")
                                   forState:UIControlStateHighlighted];
        [_forwardButton setBackgroundImage:IMGNAMED(@"btn_web_forward.png") forState:UIControlStateNormal];
        [_forwardButton setBackgroundImage:IMGNAMED(@"btn_web_forward_light.png")
                                  forState:UIControlStateHighlighted];
        [_refreshButton setBackgroundImage:IMGNAMED(@"btn_refresh.png") forState:UIControlStateNormal];
        [_forwardButton setBackgroundImage:IMGNAMED(@"btn_picrefresh.png")
                                  forState:UIControlStateHighlighted];
        [self.bottomImageView addSubview:_menuView];
    }
    return _menuView;
}

#pragma mark - UIWebViewDelegate methods
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    self.forwardButton.enabled = self.webView.canGoForward;
    self.backwardButton.enabled = self.webView.canGoBack;
	self.refreshButton.enabled = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    self.forwardButton.enabled = self.webView.canGoForward;
    self.backwardButton.enabled = self.webView.canGoBack;
	self.refreshButton.enabled = YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
	self.forwardButton.enabled = NO;
	self.backwardButton.enabled = NO;
	self.refreshButton.enabled = NO;
}

#pragma mark - UIButton method
- (void)back:(id)sender {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backwardButtonTapped:(id)sender {
    [self.webView goBack];
}

- (void)forwardButtonTapped:(id)sender {
    [self.webView goForward];
}

- (void)refreshButtonTapped:(id)sender {
    [self.webView reload];
}

#pragma mark action
- (void)toback{
    [self.webView goBack];
}
- (void)forward{
    [self.webView goForward];
}
- (void)refresh{
    [self.webView reload];
}
- (void)share{
//    [self.menu showInView:self.view];
}
- (void)backParentController{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
