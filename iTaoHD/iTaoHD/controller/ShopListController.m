//
//  ShopListController.m
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ShopListController.h"
#import "GMGridView.h"
#import "ShopGridViewCell.h"
#import "ShopKey.h"
#import "ShopViewController.h"

@interface ShopListController ()<GMGridViewDataSource,GMGridViewDelegate>

@property (nonatomic, strong) GMGridView *gmGridView;
@property (nonatomic, strong) UIImageView          *bottomImageView;
@property (nonatomic, strong) NSMutableArray *datasource;
@end

@implementation ShopListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadFromDisk];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.gmGridView reloadData];
    [self bottomImageView];
  
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadFromDisk{
    [self.datasource removeAllObjects];
    NSString *pathString = [[NSBundle mainBundle] pathForResource:@"shop" ofType:@"json"];
    NSData *elementsData = [NSData dataWithContentsOfFile:pathString];
    
    NSError *anError = nil;
    NSArray *parsedElements = [NSJSONSerialization JSONObjectWithData:elementsData
                                                              options:NSJSONReadingAllowFragments
                                                                error:&anError];
    
    for (NSDictionary *aModuleDict in parsedElements){
        ShopKey *shop = [[ShopKey alloc] init];
        [shop setFid:[[aModuleDict objectForKey:@"fid"] integerValue]];
        [shop setIcon:[aModuleDict objectForKey:@"icon"]];
        [shop setName:[aModuleDict objectForKey:@"name"]];
        [shop setDescription:[aModuleDict objectForKey:@"description"]];
        [shop setKey:[aModuleDict objectForKey:@"key"]];
        [self.datasource addObject:shop];
        
    }
   
}
#pragma mark - GMGridViewDataSource
- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return self.datasource.count;
}

- (CGSize)sizeForItemsInGMGridView:(GMGridView *)gridView
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        return CGSizeMake(182, 200);
        
    } else {
        return CGSizeMake(160, 160);
    }
}

- (GMGridViewCell *)gmGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    
    CGSize size = [self sizeForItemsInGMGridView:gridView];
    
    ShopGridViewCell *cell = (ShopGridViewCell *)[gridView dequeueReusableCell];
    
    if (!cell) {
        cell = [[ShopGridViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    ShopKey *shop = [self.datasource objectAtIndex:index];
    cell.frame = CGRectMake(0, 0, size.width, size.height);
    [cell setShop:shop];
    return cell;
}

#pragma mark - GMGridViewDelegate

- (void)gmGridView:(GMGridView *)gridView pageNumberInGridView:(NSInteger)pageNumber
{
//    NSArray * subSources = [SourceManager sharedManager].meSources;
//    // 重新计算page数
//    _totalPage = ceil([subSources count]*1.0/kIntSrcBtnsPerPage);
//    self.pageControl.numberOfPages = _totalPage;
//    self.currentPage = pageNumber;
}

- (void)gmGridView:(GMGridView *)gridView didSelectItemAtIndex:(NSInteger)index{
    ShopGridViewCell  *cell = (ShopGridViewCell  *)[gridView cellForItemAtIndex:index];
    ShopKey *shop = [self.datasource objectAtIndex:index];
    
    ShopViewController *shopVC = [[ShopViewController alloc] initWithShopKey:shop];
    [self.navigationController pushViewController:shopVC animated:YES];
}

#pragma mark - getter
- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(self.view.bounds) - kDefaultBottomBarHeight -_kSpaceHeight,
                                                                         CGRectGetWidth(self.view.bounds),
                                                                         kDefaultBottomBarHeight)];
        _bottomImageView.backgroundColor = COLOR_RGB_BOTTOM;
        _bottomImageView.userInteractionEnabled = YES;
        
        UIImage *image = [IMGNAMED(@"bg_navback.png") resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
        [_bottomImageView setImage:image];
        
        UIImage *imgBack = IMGNAMED(@"btn_back.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, floorf((_bottomImageView.bounds.size.height-imgBack.size.height)/2), imgBack.size.width, imgBack.size.height);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_bottomImageView addSubview:back];
        
        [back setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [back setBackgroundImage:IMGNAMED(@"btn_back_light.png") forState:UIControlStateHighlighted];
        [self.view addSubview:_bottomImageView];
    }
    return _bottomImageView;
}

- (NSMutableArray *)datasource{
    if (!_datasource) {
        _datasource = [[NSMutableArray alloc] initWithCapacity:20];
    }
    return _datasource;
}
- (GMGridView *)gmGridView{
    if (!_gmGridView) {
        _gmGridView = [[GMGridView alloc] initWithFrame:CGRectMake(0,
                                                                   0,
                                                                   CGRectGetWidth(self.view.bounds),
                                                                   self.view.bounds.size.height -kDefaultBottomBarHeight - _kSpaceHeight)];
        _gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _gmGridView.backgroundColor = [UIColor clearColor];
        _gmGridView.centerGrid = NO;
        _gmGridView.actionDelegate = self;
        _gmGridView.dataSource = self;
        [self.view addSubview:_gmGridView];
    }
    return _gmGridView;
}

@end
