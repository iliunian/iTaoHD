//
//  FavoritesController.m
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FavoritesController.h"
#import "FavoritManager.h"
#import "LolayGridView.h"
#import "ItemCell.h"
#import "ItemViewController.h"
#import "Favorit.h"

#define kLandscapeNum    5

@interface FavoritesController ()<LolayGridViewDataSource,LolayGridViewDelegate,LolayGridViewCellDelegate,FavoritManagerDelegate>{
    BOOL    _isEditing;
}
@property (nonatomic, strong) LolayGridView *gridView;
@property (nonatomic, strong) NSMutableArray    *datasource;
@property (nonatomic, strong) UIImageView  *bottomImageView;
@property (nonatomic, strong) UIButton  *editButton;
@end

@implementation FavoritesController

- (void)dealloc{
    [[FavoritManager sharedManager] removeObserver:self];
}
- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        [[FavoritManager sharedManager] addObserver:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:IMGNAMED(@"wall_filter_bg_body.png")];;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self bottomImageView];
    [self gridView];
    [[FavoritManager sharedManager] getFavorites];
    
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)doedit{
    _isEditing = !_isEditing;
    [self.gridView setEditing:_isEditing];
    NSString *editTitle =_isEditing?@"完成": @"编辑";
    [self.editButton setTitle:editTitle forState:UIControlStateNormal];
//    [self.gridView reloadData];
}
#pragma mark FavoritManagerDelegate
- (void)mkManagerFavoritesDidFinishedWithFavorites:(NSMutableArray *)favorites{
    if (favorites) {
        [self.datasource addObjectsFromArray:favorites];
        [self.gridView reloadData];
    }
}
#pragma mark LolayGridViewDataSource
- (NSInteger)numberOfRowsInGridView:(LolayGridView*)gridView
{
    NSInteger count = [self.datasource count];
    return ceil(1.0f*count/kLandscapeNum);
}
- (NSInteger)numberOfColumnsInGridView:(LolayGridView*)gridView
{
    return kLandscapeNum;
}

- (LolayGridViewCell*)gridView:(LolayGridView*)gridView
                    cellForRow:(NSInteger)gridRowIndex
                      atColumn:(NSInteger)gridColumnIndex
{
    CGFloat width = CGRectGetWidth(self.view.bounds) /kLandscapeNum;
    NSInteger objectIndex = gridRowIndex * kLandscapeNum + gridColumnIndex;
    if (objectIndex >= [self.datasource count]) {
        return nil;
    }
    
    static NSString *cellID = @"newsCell";
    ItemCell *cell = (ItemCell *)[gridView dequeueReusableGridCellWithIdentifier:cellID];
    
    if (!cell) {
        cell = [[ItemCell alloc] initWithFrame:CGRectMake(0, 0, width, width + kDefaultCellBottomBarHeight) reuseIdentifier:cellID];
    }
//    [cell setEditing:gridView.editing];
    cell.delegate = self;
    Favorit *favorit = (Favorit *)[self.datasource objectAtIndex:objectIndex];
    CuzyTBKItem *item = favorit.item;
    [cell updateWithItem:item];
    return cell;
}

#pragma mark LolayGridViewDelegate
- (CGFloat)heightForGridViewRows:(LolayGridView*)gridView
{
    return CGRectGetWidth(self.view.bounds) /kLandscapeNum + kDefaultCellBottomBarHeight;
}
- (CGFloat)widthForGridViewColumns:(LolayGridView*)gridView
{
    return CGRectGetWidth(self.view.bounds) /kLandscapeNum;;
}
- (void)gridView:(LolayGridView*)gridView
didSelectCellAtRow:(NSInteger)gridRowIndex
        atColumn:(NSInteger)gridColumnIndex
{
    
    NSInteger objectIndex = gridRowIndex * kLandscapeNum + gridColumnIndex;
    
    ItemCell *cell = (ItemCell *)[gridView cellForRow:gridRowIndex atColumn:gridColumnIndex];
    
    Favorit *favorit = (Favorit *)[self.datasource objectAtIndex:objectIndex];
    CuzyTBKItem *item = favorit.item;
    ItemViewController *itemVC = [[ItemViewController alloc] initWithMeItem:item];
    itemVC.shareImage = cell.srcImageView.image;
    [self.navigationController pushViewController:itemVC animated:YES];
}
- (void)gridView:(LolayGridView*)gridView willDeleteCellAtRow:(NSInteger)gridRowIndex atColumn:(NSInteger)gridColumnIndex{
    NSInteger objectIndex = gridRowIndex * kLandscapeNum + gridColumnIndex;
    Favorit *favorit = (Favorit *)[self.datasource objectAtIndex:objectIndex];
    CuzyTBKItem *item = favorit.item;

    [[FavoritManager sharedManager] removeItem:item];
    [self.datasource removeObject:favorit];
}
- (void)gridView:(LolayGridView*)gridView didDeleteCellAtRow:(NSInteger)gridRowIndex atColumn:(NSInteger)gridColumnIndex{
}

#pragma mark LolayGridViewCellDelegate
- (void)didSelectGridCell:(LolayGridViewCell*)gridCellView{

}
- (void)deleteButtonSelectedForGridCell:(LolayGridViewCell*)gridCellView{

}
#pragma mark - getter
- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(self.view.bounds) - kDefaultBottomBarHeight -_kSpaceHeight,
                                                                         CGRectGetWidth(self.view.frame),
                                                                         kDefaultBottomBarHeight)];
        _bottomImageView.userInteractionEnabled = YES;
        _bottomImageView.backgroundColor = [UIColor clearColor];
        UIImage *image = [IMGNAMED(@"bg_navback.png") resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
        [_bottomImageView setImage:image];
        
        UIImage *imgBack = IMGNAMED(@"btn_back.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, floorf((_bottomImageView.bounds.size.height-imgBack.size.height)/2), imgBack.size.width, imgBack.size.height);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_bottomImageView addSubview:back];
        
        [back setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [back setBackgroundImage:IMGNAMED(@"btn_back_light.png") forState:UIControlStateHighlighted];

        [self.editButton setFrame:CGRectMake(CGRectGetMaxX(back.frame) + 100, 11, 76, 36)];
        [_bottomImageView addSubview:self.editButton];
        [self.view addSubview:_bottomImageView];
    }
    return _bottomImageView;
}

- (UIButton *)editButton{
    if (!_editButton) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setTitle:@"编辑" forState:UIControlStateNormal];
        [_editButton setTitleColor:[UIColor flatBlackColor] forState:UIControlStateNormal];
        [_editButton setBackgroundImage:IMGNAMED(@"bg_nav_button_small.png") forState:UIControlStateNormal];
        [_editButton addTarget:self action:@selector(doedit) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editButton;
}

- (NSMutableArray *)datasource{
    if (!_datasource) {
        _datasource = [[NSMutableArray alloc] initWithCapacity:20];
    }
    return _datasource;
}

- (LolayGridView *)gridView{
    if (!_gridView) {
        _gridView = [[LolayGridView alloc] initWithFrame:CGRectMake(0,
                                                                    0,
                                                                    CGRectGetWidth(self.view.bounds),
                                                                    CGRectGetHeight(self.view.bounds) - 58-_kSpaceHeight)];
        _gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _gridView.backgroundColor = [UIColor colorWithRed:160/225.0 green:160/225.0 blue:130/225.0 alpha:1.0];
        _gridView.backgroundColor = [UIColor colorWithPatternImage:IMGNAMED(@"wall_filter_bg_body.png")];
        _gridView.topBounceEnabled = YES;
        _gridView.dataDelegate = self;
        _gridView.dataSource = self;
        _gridView.delegate = self;
        _gridView.showsVerticalScrollIndicator = NO;
        _gridView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:_gridView];
    }
    return _gridView;
}
@end
