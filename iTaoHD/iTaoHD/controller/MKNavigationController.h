//
//  MKNavigationController.h
//  Mooker
//
//  Created by lvmeng on 12-9-21.
//
//

#import <UIKit/UIKit.h>

@interface MKNavigationController : UINavigationController

@property (nonatomic, assign) BOOL shouldRotate;

@end
