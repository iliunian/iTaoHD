//
//  WebViewController.m
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "WebViewController.h"
#import "LocalSubstitutionCache.h"
#import "MBButtonMenuViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kDefaultMenuViewHeight            44
#define kDefaultTopBarHeight              39
#define kDefaultColorHeadHeight           3
#define COLOR_RGB_BOTTOM     [UIColor colorWithRed:234.0/255.0f green:234.0/255.0f blue:234.0/255.0f alpha:1.0]
@interface WebViewController ()<UIWebViewDelegate,MFMailComposeViewControllerDelegate,MBButtonMenuViewControllerDelegate>
@property (nonatomic, retain) NSURL *URL;
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) UIView    *menuView;

@property (nonatomic, retain) UIImageView *colorHeadImage;
@property (nonatomic, retain) UIButton  *backButton;
@property (nonatomic, retain) UIButton  *backwardButton;
@property (nonatomic, retain) UIButton  *forwardButton;
@property (nonatomic, retain) UIButton  *refreshButton;

@property (nonatomic, strong) MBButtonMenuViewController *menu;
@end

@implementation WebViewController                                                                                                                                                                                                                                                                                                                                                                                            
- (void)dealloc{
    _webView.delegate = nil;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
- (id)initWithURL:(NSURL*)pageURL {
    
    if(self = [super init]) {
        self.view.backgroundColor = [UIColor whiteColor];
        self.URL = pageURL;
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.colorHeadImage];
    [self.view addSubview:self.webView];
    [self.view addSubview:self.menuView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.URL]];
}


- (UIImageView *)colorHeadImage
{
    if (!_colorHeadImage)
        {
        _colorHeadImage = [[UIImageView alloc] initWithImage:IMGNAMED(@"bg_colorhead.png")];
        _colorHeadImage.frame = CGRectMake(0, 0, self.view.bounds.size.width, kDefaultColorHeadHeight);
        }
    return _colorHeadImage;
}

- (UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, kDefaultColorHeadHeight,
                                                               self.view.bounds.size.width,
                                                               self.view.bounds.size.height - kDefaultBottomBarHeight - kDefaultColorHeadHeight)];
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _webView.scalesPageToFit = YES;
        _webView.delegate = self;
    }
    return _webView;
}

- (UIView *)menuView {
    if (!_menuView) {
        _menuView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - kDefaultBottomBarHeight, self.view.bounds.size.width, kDefaultBottomBarHeight)];
        _menuView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.exclusiveTouch = YES;
        UIImage *image = IMGNAMED(@"btn_back.png");
        _backButton.frame = CGRectMake(10,
                                       floor((_menuView.bounds.size.height-image.size.height)/2),
                                       image.size.width,
                                       image.size.height);
        [_backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        self.backButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        [_menuView addSubview:_backButton];
        
        _backwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backwardButton.exclusiveTouch = YES;
        image = IMGNAMED(@"btn_web_backward.png");
        _backwardButton.frame = CGRectMake(floor(_menuView.bounds.size.width/2)-40-image.size.width,
                                           floor((_menuView.bounds.size.height-image.size.height)/2),
                                           image.size.width,
                                           image.size.height);
        [_backwardButton addTarget:self action:@selector(backwardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _backwardButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        [_menuView addSubview:_backwardButton];
        
        _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _forwardButton.exclusiveTouch = YES;
        image = IMGNAMED(@"btn_web_forward.png");
        _forwardButton.frame = CGRectMake(floor(_menuView.bounds.size.width/2)+10,
                                          floor((_menuView.bounds.size.height-image.size.height)/2),
                                          image.size.width,
                                          image.size.height);
        [_forwardButton addTarget:self action:@selector(forwardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _forwardButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
        [_menuView addSubview:_forwardButton];
        
        _refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _refreshButton.exclusiveTouch = YES;
        image = IMGNAMED(@"btn_web_refresh.png");
        _refreshButton.frame = CGRectMake(_menuView.bounds.size.width- 2 * (10+image.size.width),
                                          floor((_menuView.bounds.size.height-image.size.height)/2),
                                          image.size.width,
                                          image.size.height);
        [_refreshButton addTarget:self action:@selector(refreshButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _refreshButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
        [_menuView addSubview:_refreshButton];
        
        
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.exclusiveTouch = YES;
        image = IMGNAMED(@"btn_forward.png");
        shareButton.frame = CGRectMake(self.view.bounds.size.width - 10 - image.size.width,
                                       floor((kDefaultBottomBarHeight-image.size.height)/2),
                                       image.size.width, image.size.height);
        [shareButton addTarget:self
                        action:@selector(share)
              forControlEvents:UIControlEventTouchUpInside];
        shareButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [_menuView addSubview:shareButton];
        
        _menuView.backgroundColor = COLOR_RGB_BOTTOM;
        [_backButton setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [_backButton setBackgroundImage:IMGNAMED(@"btn_back_light.png")
                               forState:UIControlStateHighlighted];
        [_backwardButton setBackgroundImage:IMGNAMED(@"btn_web_backward.png") forState:UIControlStateNormal];
        [_backwardButton setBackgroundImage:IMGNAMED(@"btn_web_backward_light.png")
                                   forState:UIControlStateHighlighted];
        [_forwardButton setBackgroundImage:IMGNAMED(@"btn_web_forward.png") forState:UIControlStateNormal];
        [_forwardButton setBackgroundImage:IMGNAMED(@"btn_web_forward_light.png")
                                  forState:UIControlStateHighlighted];
        [_refreshButton setBackgroundImage:IMGNAMED(@"btn_web_refresh.png") forState:UIControlStateNormal];
        [_refreshButton setBackgroundImage:IMGNAMED(@"btn_web_refresh_light.png")
                                  forState:UIControlStateHighlighted];
        [shareButton setBackgroundImage:IMGNAMED(@"btn_forward.png") forState:UIControlStateNormal];
        [shareButton setBackgroundImage:IMGNAMED(@"btn_forward_light.png") forState:UIControlStateHighlighted];
    }
    return _menuView;
}

#pragma mark - UIWebViewDelegate methods
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    self.forwardButton.enabled = self.webView.canGoForward;
    self.backwardButton.enabled = self.webView.canGoBack;
	self.refreshButton.enabled = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    self.forwardButton.enabled = self.webView.canGoForward;
    self.backwardButton.enabled = self.webView.canGoBack;
	self.refreshButton.enabled = YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
	self.forwardButton.enabled = NO;
	self.backwardButton.enabled = NO;
	self.refreshButton.enabled = NO;
}

#pragma mark - UIButton method
- (void)back:(id)sender {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backwardButtonTapped:(id)sender {
    [self.webView goBack];
}

- (void)forwardButtonTapped:(id)sender {
    [self.webView goForward];
}

- (void)refreshButtonTapped:(id)sender {
    [self.webView reload];
}

#pragma mark - MBButtonMenuViewControllerDelegate
- (void)buttonMenuViewController:(MBButtonMenuViewController *)buttonMenu buttonTappedAtIndex:(NSUInteger)index
{
    [buttonMenu hide];
    switch (index) {
        case 0:
        {
        [[UIApplication sharedApplication] openURL:self.webView.request.URL];
        }
            break;
        case 1:
        {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = self.webView.request.URL.absoluteString;
        }
            break;
        case 2:
        {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:[self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
        [mailViewController setMessageBody:self.webView.request.URL.absoluteString isHTML:NO];
        mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
    
}

- (void)buttonMenuViewControllerDidCancel:(MBButtonMenuViewController *)buttonMenu{
    [buttonMenu hide];
}
#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark action
- (void)back{
    [self.webView goBack];
}
- (void)forward{
    [self.webView goForward];
}
- (void)refresh{
    [self.webView reload];
}
- (void)share{
    [self.menu showInView:self.view];
}
- (void)backParentController{
    [self.navigationController popViewControllerAnimated:YES];
}

- (MBButtonMenuViewController *)menu{
    if (nil == _menu) {
        NSArray *titles = [NSArray arrayWithObjects:@"在Safari中打开",
                           @"拷贝该页网址",
                           @"邮件发送该网址",
                           @"取消", nil];
        _menu = [[MBButtonMenuViewController alloc] init];
        [_menu setButtonTitles:titles];
        [_menu setDelegate:self];
        [_menu setCancelButtonIndex:[[_menu buttonTitles] count]-1];
    }
    return _menu;
}

@end
