//
//  OptionViewController.m
//  iMiniTao
//
//  Created by liunian on 13-8-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "OptionViewController.h"
#import "DAAppsViewController.h"
#import "UMFeedbackViewController.h"
#import "UMFeedback.h"
#import "FqViewController.h"
#import "ALAlertBanner.h"
#import "AppDelegate.h"
#import "DesktopController.h"


@interface OptionViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (nonatomic, retain) UIImageView          *navImageView;

@property (nonatomic, retain) UITableView   *tableView;
@property (nonatomic, retain) UIView        *headView;
@end

@implementation OptionViewController
- (void)dealloc{
    BMLog(@"dealloc");
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_RGB(248, 248, 248);
    
    [self.view addSubview:self.navImageView];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [MobClick beginLogPageView:@"选项视图"];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"选项视图"];
}
- (void)back{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(long)fileSizeForDir:(NSString*)path//计算文件夹下文件的总大小
{
    long size = 0;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray* array = [fileManager contentsOfDirectoryAtPath:path error:nil];
    for(int i = 0; i<[array count]; i++)
    {
        NSString *fullPath = [path stringByAppendingPathComponent:[array objectAtIndex:i]];
        
        BOOL isDir;
        if ( !([fileManager fileExistsAtPath:fullPath isDirectory:&isDir] && isDir) )
        {
            NSDictionary *fileAttributeDic = [fileManager attributesOfItemAtPath:fullPath error:nil];
            size += fileAttributeDic.fileSize;
        }
        else
        {
            [self fileSizeForDir:fullPath];
        }
    }
    return size;
    
}
//单位:MB
- (NSString *)sizeCache{
    NSArray*paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    NSString*cachesDir = [paths objectAtIndex:0];
    NSString *ImageCachePath = [cachesDir stringByAppendingPathComponent:@"ImageCache"];
    long ImageCacheSize = [self fileSizeForDir:ImageCachePath];
    
    NSString *urlCachePath = [cachesDir stringByAppendingPathComponent:@"URLCACHE"];
    long urlCacheSize = [self fileSizeForDir:urlCachePath];
    
    long totalSize = ImageCacheSize + urlCacheSize;
    const unsigned int bytes = 1024*1024 ;   //字节数，如果想获取KB就1024，MB就1024*1024
    NSString *string = [NSString stringWithFormat:@"%.2f",(1.0 *totalSize/bytes)];
    BMLog(@"ImageCacheSize:%ld,urlCacheSize:%ld",ImageCacheSize,urlCacheSize);
    return string;
}
- (BOOL)cleanAllCache{
    [[SDImageCache sharedImageCache] clearDisk];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES) objectAtIndex:0];
    NSString *urlCache_doc = [cachePath stringByAppendingPathComponent:@"URLCACHE"];
    
    BOOL result = [[NSFileManager defaultManager] removeItemAtPath:urlCache_doc error:nil];
    
    if (result ){
        return YES;
    }
    return NO;
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        return;
    }
    switch (alertView.tag) {
        case 10011:
        {
        }
            break;
        case 10012:
        {
        }
            break;
        case 10013:
        {
        ALAlertBannerStyle randomStyle = ALAlertBannerStyleSuccess;
        NSString *aTitle = @"成功";
        NSString *aContent = @"缓存清理成功!";
        if (![self cleanAllCache]) {
            aTitle = @"失败";
            aContent = @"清理缓存失败,请重试!";
            randomStyle = ALAlertBannerStyleFailure;
        }
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        ALAlertBannerPosition position = ALAlertBannerPositionUnderNavBar;
        ALAlertBanner *banner = [ALAlertBanner alertBannerForView:appDelegate.window style:randomStyle position:position title:aTitle subtitle:aContent tappedBlock:^(ALAlertBanner *alertBanner) {
            [alertBanner hide];
        }];
        banner.secondsToShow = 2;
        banner.showAnimationDuration = 0.25;
        banner.hideAnimationDuration = 0.20;
        [banner show];

            [self.tableView reloadData];
        }
            break;
            
        default:
            break;
    }
}
- (void)showNativeFeedbackWithAppkey:(NSString *)appkey {
    UMFeedbackViewController *feedbackViewController = [[UMFeedbackViewController alloc] initWithNibName:@"UMFeedbackViewController" bundle:nil];
    feedbackViewController.appkey = appkey;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:feedbackViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return 3;
        }
            break;
        case 1:
        {
            return 3;
        }
            break;
        default:
            break;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
        {
            return @"程序";
        }
            break;
        case 1:
        {
            return @"其他";
        }
            break;
            
        default:
            break;
    }
    return @"其他";
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    switch (indexPath.section) {

        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell.textLabel.text = @"清理缓存文件";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@MB",[self sizeCache]];
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"帮助说明";
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                }
                    break;
                case 2:
                {
                cell.textLabel.text = @"设置桌面背景";
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell.textLabel.text = @"给我们打分";
                }
                    break;
                case 1:
                {
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    cell.textLabel.text = @"版本更新";
                }
                    break;
                case 2:
                {
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    cell.textLabel.text = @"反馈和建议";
                
                    UMFeedback *feedback = [UMFeedback sharedInstance];
                    if (feedback.newReplies) {
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"有%d条新回复",[feedback.newReplies count]];
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}


#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定清理缓存文件么?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    [alert setTag:10013];
                    [alert show];
                    
                }
                    break;
                case 1:
                {
                    FqViewController *fqVC = [[FqViewController alloc] init];
                    [self.navigationController pushViewController:fqVC animated:YES];
                }
                    break;
                case 2:
                {
                    DesktopController *desktopVC = [[DesktopController alloc] init];
                    [self.navigationController pushViewController:desktopVC animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    NSString *appUrlString = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%u?mt=8",kAPPID];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appUrlString]];
                
                }
                    break;
                case 1:
                {
                    [MobClick checkUpdateWithDelegate:self selector:@selector(callBackSelectorWithDictionary:)];
                   
                }
                    break;
                case 2:
                {
                    [self showNativeFeedbackWithAppkey:kUmengAppKey];

                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    
}

- (void)callBackSelectorWithDictionary:(NSDictionary *)appUpdateInfo{
    BMLog(@"%@",appUpdateInfo);
    BOOL update = [[appUpdateInfo objectForKey:@"update"] boolValue];
    if (update) {
        [MobClick checkUpdate:@"有新版本更新啦！" cancelButtonTitle:@"我爱怀旧" otherButtonTitles:@"我爱潮流"];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            ALAlertBannerStyle randomStyle = ALAlertBannerStyleSuccess;
            ALAlertBannerPosition position = ALAlertBannerPositionUnderNavBar;
            ALAlertBanner *banner = [ALAlertBanner alertBannerForView:appDelegate.window style:randomStyle position:position title:@"提示" subtitle:@"您使用的已经是最新的版本!" tappedBlock:^(ALAlertBanner *alertBanner) {
                NSLog(@"tapped!");
                [alertBanner hide];
            }];
            banner.secondsToShow = 2;
            banner.showAnimationDuration = 0.25;
            banner.hideAnimationDuration = 0.20;
            [banner show];
            return;
            
        });
    }
    
}

#pragma mark - getter
- (UIImageView *)navImageView{
    if (!_navImageView) {
        UIImage  *img = IMGNAMED(@"bg_nav_modal.png");
        img = [img stretchableImageWithLeftCapWidth:floor(img.size.width/2) topCapHeight:0];
        _navImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 540, 63)];
        _navImageView.backgroundColor = [UIColor clearColor];
        _navImageView.userInteractionEnabled = YES;
        _navImageView.image = img;
        
        UILabel  *title = [[UILabel alloc] initWithFrame:CGRectMake((_navImageView.bounds.size.width-200)/2,
                                                                    0, 200, _navImageView.bounds.size.height)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = COLOR_RGB(0, 0, 0);
        title.textAlignment = ALIGNTYPE_CENTER;
        title.font = FONT_TITLE(26.0f);
        title.text = @"设置";
        [_navImageView addSubview:title];
        
        UIImage *imgBtn = IMGNAMED(@"bg_nav_button_small.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, (_navImageView.bounds.size.height-imgBtn.size.height)/2,
                                imgBtn.size.width, imgBtn.size.height);
        [back setBackgroundImage:imgBtn forState:UIControlStateNormal];
        [back setTitleColor:COLOR_RGB(0,0,0) forState:UIControlStateNormal];
        [back setTitle:@"关闭" forState:UIControlStateNormal];
        back.titleLabel.font = FONT_CONTENT(16.0f);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_navImageView addSubview:back];
    }
    return _navImageView;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                   self.navImageView.frame.origin.y+self.navImageView.bounds.size.height,
                                                                   self.navImageView.bounds.size.width,
                                                                   620-self.navImageView.bounds.size.height)
                      style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView setBackgroundView:nil];
        [_tableView setBackgroundColor:[UIColor clearColor]];
        [_tableView setTableHeaderView:self.headView];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (UIView *)headView{
    if (!_headView) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.navImageView.bounds.size.width, 150)];
        
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(_headView.frame) - 72)/2, 20, 72, 72)];
        logo.userInteractionEnabled = YES;
        logo.backgroundColor = [UIColor clearColor];
        [logo setImage:IMGNAMED(@"Icon-72.png")];
        [_headView addSubview:logo];
        
        logo.layer.shadowColor = [UIColor grayColor].CGColor;
        logo.layer.shadowOffset = CGSizeMake(0, -1);
        logo.layer.shadowOpacity = 0.5;
        logo.layer.shadowRadius = 5.0;
        
        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, 500, 30)];
        [name setBackgroundColor:[UIColor clearColor]];
        [name setLineBreakMode:NSLineBreakByCharWrapping];
        [name setNumberOfLines:0];
        [name setText:[NSString stringWithFormat:@"淘淘看"]];
        [name setFont:[UIFont systemFontOfSize:18]];
        [name setTextAlignment:[Util getAlign:ALIGNTYPE_CENTER]];
        [name setTextColor:[UIColor flatDarkBlackColor]];
        [_headView addSubview:name];
        
        UILabel *version = [[UILabel alloc] initWithFrame:CGRectMake(20, 130, 500, 20)];
        [version setBackgroundColor:[UIColor clearColor]];
        [version setLineBreakMode:NSLineBreakByCharWrapping];
        [version setNumberOfLines:0];
        [version setText:[NSString stringWithFormat:@"Ver %@",APP_VERSION]];
        [version setFont:[UIFont systemFontOfSize:14]];
        [version setTextAlignment:[Util getAlign:ALIGNTYPE_CENTER]];
        [version setTextColor:[UIColor flatBlackColor]];
        [_headView addSubview:version];
    }
    return _headView;
}
@end
