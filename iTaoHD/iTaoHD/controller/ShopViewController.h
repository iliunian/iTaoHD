//
//  ShopViewController.h
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ViewController.h"

@class ShopKey;
@interface ShopViewController : ViewController
- (id)initWithShopKey:(ShopKey *)shopKey;
@end
