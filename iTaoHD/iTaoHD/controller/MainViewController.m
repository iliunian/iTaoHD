//
//  MainViewController.m
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController () <UIGestureRecognizerDelegate>
{
    CGRect _orignFrame;
}

@property (nonatomic, retain) UIPanGestureRecognizer *panGesture;

- (void)orientationChanged:(UIInterfaceOrientation)currentOrientation;

- (void)handleAppDidEnterBackground:(NSNotification *)notification;
- (void)handleAppWillEnterForeground:(NSNotification *)notifcation;
@end

@implementation MainViewController

- (id)init {
    self = [super init];
    if (self) {
        [DataEnvironment sharedDataEnvironment].mainViewController = self;        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleAppDidEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleAppWillEnterForeground:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //初始化sourceView
    if (!_sourceView) {
        [self.view addSubview:self.sourceView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.mainViewShown = YES;
    [self.sourceView sourceViewDidAppear];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.mainViewShown = NO;
    [self.sourceView sourceviewWillDisappear];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self orientationChanged:[UIApplication sharedApplication].statusBarOrientation];
}

#pragma mark - handle notifications
- (void)handleAppDidEnterBackground:(NSNotification *)notification {

}

- (void)handleAppWillEnterForeground:(NSNotification *)notifcation {

}

- (SourceView *)sourceView {
    if (!_sourceView) {
        _sourceView = [[SourceView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds),  CGRectGetHeight(self.view.bounds))];
        _sourceView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return _sourceView;
}

#pragma mark -
#pragma mark  handle orientation change
- (void)orientationChanged:(UIInterfaceOrientation)currentOrientation
{
//    _sourceView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
}

#pragma mark - UIGestureRecognizer handler method
- (void)setSlideEnabled:(BOOL)slideEnabled
{
    _slideEnabled = slideEnabled;
    self.panGesture.enabled = _slideEnabled;
}
@end
