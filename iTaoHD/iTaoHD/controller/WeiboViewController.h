//
//  WeiboViewController.h
//  iTaoHD
//
//  Created by liunian on 13-12-6.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeiboViewController : UIViewController
- (id)initWithCuzyItem:(CuzyTBKItem *)item image:(UIImage *)image infoString:(NSString *)infoString;
@end
