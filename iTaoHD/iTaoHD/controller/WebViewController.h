//
//  WebViewController.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface WebViewController : UIViewController
- (id)initWithURL:(NSURL*)URL;
@end
