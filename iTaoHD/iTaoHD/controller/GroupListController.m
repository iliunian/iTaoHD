//
//  GroupListController.m
//  iTaoHD
//
//  Created by liunian on 13-11-29.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "GroupListController.h"

@interface GroupListController ()

@end

@implementation GroupListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
