//
//  FqViewController.m
//  iMiniTao
//
//  Created by liunian on 13-8-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FqViewController.h"
#import "WebViewController.h"

#define kFQWebURL   @"http://iminitao.com/app/fq.html"

@interface FqViewController ()<UIWebViewDelegate>
@property (nonatomic, retain) UIImageView          *navImageView;
@property (nonatomic, retain) UIWebView     *webView;
@property (nonatomic, retain) UIActivityIndicatorView *indicatorView;
@end

@implementation FqViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_RGB(248, 248, 248);
    
    [self.view addSubview:self.navImageView];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:kFQWebURL]];
    if (request) {
        [self.indicatorView startAnimating];
        [self.webView loadRequest:request];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.indicatorView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.indicatorView stopAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        WebViewController *webVC = [[WebViewController alloc] initWithURL:request.URL];
        [self.navigationController pushViewController:webVC animated:YES];
        return NO;
    }
    return YES;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.indicatorView stopAnimating];
}
#pragma mark - getter
- (UIImageView *)navImageView{
    if (!_navImageView) {
        UIImage  *img = IMGNAMED(@"bg_nav_modal.png");
        img = [img stretchableImageWithLeftCapWidth:floor(img.size.width/2) topCapHeight:0];
        _navImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 540, 63)];
        _navImageView.backgroundColor = [UIColor clearColor];
        _navImageView.userInteractionEnabled = YES;
        _navImageView.image = img;
        
        UILabel  *title = [[UILabel alloc] initWithFrame:CGRectMake((_navImageView.bounds.size.width-200)/2,
                                                                    0, 200, _navImageView.bounds.size.height)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = COLOR_RGB(0, 0, 0);
        title.textAlignment = ALIGNTYPE_CENTER;
        title.font = FONT_TITLE(26.0f);
        title.text = @"频道订阅";
        [_navImageView addSubview:title];
        
        UIImage *imgBtn = IMGNAMED(@"bg_nav_button_small.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, (_navImageView.bounds.size.height-imgBtn.size.height)/2,
                                imgBtn.size.width, imgBtn.size.height);
        [back setBackgroundImage:imgBtn forState:UIControlStateNormal];
        [back setTitleColor:COLOR_RGB(0,0,0) forState:UIControlStateNormal];
        [back setTitle:@"返回" forState:UIControlStateNormal];
        back.titleLabel.font = FONT_CONTENT(16.0f);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_navImageView addSubview:back];
        
    }
    return _navImageView;
}
- (UIActivityIndicatorView *)indicatorView{
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _indicatorView.hidesWhenStopped = YES;
    }
    return _indicatorView;
}


- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,
                                                               self.navImageView.frame.origin.y+self.navImageView.bounds.size.height,
                                                               540,
                                                               620-self.navImageView.bounds.size.height)];
        _webView.scalesPageToFit = NO;
        _webView.delegate = self;
//        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//        _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_webView];
    }
    return _webView;
}
@end
