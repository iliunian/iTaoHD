//
//  FavorityEntity.m
//  iTaoHD
//
//  Created by liunian on 13-11-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FavorityEntity.h"


@implementation FavorityEntity

@dynamic sortid;
@dynamic itemID;
@dynamic itemName;
@dynamic itemPrice;
@dynamic tradingVolumeInThirtyDays;
@dynamic itemClickURLString;
@dynamic itemImageURLString;
@dynamic promotionPrice;

@end
