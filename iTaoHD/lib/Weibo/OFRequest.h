//
//  OFRequest.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    kOFRequestPostDataTypeNone,
	kOFRequestPostDataTypeNormal,			// for normal data post, such as "user=name&password=psd"
	kOFRequestPostDataTypeMultipart,        // for uploading images and files.
}OFRequestPostDataType;


@class OFRequest;

@protocol OFRequestDelegate <NSObject>
@optional
- (void)request:(OFRequest *)request didReceiveResponse:(NSURLResponse *)response;
- (void)request:(OFRequest *)request didFailWithError:(NSError *)error;
- (void)request:(OFRequest *)request didFinishLoadingWithResult:(NSString *)result;

@end

@interface OFRequest : NSObject{
    NSString                *url;
    NSString                *httpMethod;
    NSDictionary            *params;
    OFRequestPostDataType   postDataType;
    NSDictionary            *httpHeaderFields;
    
    NSURLConnection         *connection;
    NSMutableData           *responseData;
    
    id<OFRequestDelegate>   delegate;
}

@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *httpMethod;
@property (nonatomic, retain) NSDictionary *params;
@property OFRequestPostDataType postDataType;
@property (nonatomic, retain) NSDictionary *httpHeaderFields;
@property (nonatomic, retain) id<OFRequestDelegate> delegate;

+ (OFRequest *)requestWithURL:(NSString *)url
                   httpMethod:(NSString *)httpMethod
                       params:(NSDictionary *)params
                 postDataType:(OFRequestPostDataType)postDataType
             httpHeaderFields:(NSDictionary *)httpHeaderFields
                     delegate:(id<OFRequestDelegate>)delegate;

+ (OFRequest *)requestWithAccessToken:(NSString *)accessToken
                                  url:(NSString *)url
                           httpMethod:(NSString *)httpMethod
                               params:(NSDictionary *)params
                         postDataType:(OFRequestPostDataType)postDataType
                     httpHeaderFields:(NSDictionary *)httpHeaderFields
                             delegate:(id<OFRequestDelegate>)delegate;

+ (NSString *)getParamValueFromUrl:(NSString*)url paramName:(NSString *)paramName;
+ (NSString *)serializeURL:(NSString *)baseURL
                    params:(NSDictionary *)params
                httpMethod:(NSString *)httpMethod;

- (void)connect;
- (void)disconnect;
@end
