//
//  OFEngineOauth2.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFEngine.h"
#import "OFAuthorizeOauth2.h"

@interface OFEngineOauth2 : OFEngine <OFAuthorizeOauth2DataSource,OFAuthorizeOauth2Delegate> {
    NSString * _accessToken;
    NSTimeInterval  _expireTime;
}

@property (nonatomic,retain) NSString * accessToken;
@property (nonatomic,assign) NSTimeInterval  expireTime;

- (id)initWithAppKey:(NSString *)theAppKey appSecret:(NSString *)theAppSecret type:(OFTypeE)type;

- (OFRequest *)generateRequestWithUrl:(NSString *)url
                           httpMethod:(NSString *)httpMethod
                               params:(NSMutableDictionary *)params
                         postDataType:(OFRequestPostDataType)postDataType
                     httpHeaderFields:(NSDictionary *)httpHeaderFields
                             tokenKey:(NSString *)tokenKey
                             delegate:(id<OFRequestDelegate>)delegate;
@end
