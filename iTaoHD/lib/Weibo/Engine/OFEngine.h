//
//  OFEngine.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OFRequest.h"
#import "OFAuthorize.h"
#import "OFSDKGlobal.h"
#import "JSON.h"
#import "OFUtil.h"
#import "Define.h"

#define kNotificationWBAuthorizeChanged [NSString stringWithFormat:@"kNotificationWBAuthorizeChanged_%@", [self urlSchemeString]]

@class OFEngine;

@protocol OFEngineDelegate <NSObject>

@optional

/*
 * If you try to log in with logIn or logInUsingUserID method, and
 * there is already some authorization info in the Keychain,
 * this method will be invoked.
 * You may or may not be allowed to continue your authorization,
 * which depends on the value of isUserExclusive.
 */
- (void)engineAlreadyLoggedIn:(OFEngine *)engine;

// Log in successfully.
- (void)engineDidLogIn:(OFEngine *)engine;

/*
 * Failed to log in.
 * Possible reasons are:
 * 1) Either username or password is wrong;
 * 2) Your app has not been authorized by open platform yet.
 */
- (void)engine:(OFEngine *)engine didFailToLogInWithError:(NSError *)error;

// Log out successfully.
- (void)engineDidLogOut:(OFEngine *)engine;


/*
 *When you use the OFEngine's request methods,
 *you may receive the following four callbacks.
 */
- (void)engineNotAuthorized:(OFEngine *)engine;
- (void)engineAuthorizeExpired:(OFEngine *)engine;

- (void)engine:(OFEngine *)engine requestDidFailWithError:(NSError *)error;
- (void)engine:(OFEngine *)engine requestDidSucceedWithResult:(id)result;

@end


@interface OFEngine : NSObject<OFAuthorizeDelegate, OFRequestDelegate> {
    NSString *_appKey;
    NSString *_appSecret;
    
    OFTypeE      _type;
    OFOauthVer  _oauthVer;
    
    id<OFEngineDelegate> _delegate;
    OFAuthorize * _authorize;
    
    NSMutableArray * _requestAry;
}

@property (nonatomic,retain) NSString *appKey;
@property (nonatomic,retain) NSString *appSecret;
@property (nonatomic,assign) id<OFEngineDelegate> delegate;
@property (nonatomic,retain) OFAuthorize *authorize;

// Initialize an instance with the AppKey and the AppSecret you have for your client.
- (id)initWithAppKey:(NSString *)theAppKey
           appSecret:(NSString *)theAppSecret
            oauthVer:(OFOauthVer)theOauthVer
                type:(OFTypeE)type;

// Log in using OAuth Web authorization.
// If succeed, engineDidLogIn will be called.
- (void)logIn;

// Log out.
// If succeed, engineDidLogOut will be called.
- (void)logOut;

// Check if user has logged in, or the authorization is expired.
- (BOOL)isLoggedIn;
- (BOOL)isAuthorizeExpired;

// @methodName: The interface you are trying to visit, exp, "statuses/public_timeline.json" for the newest timeline.
// See
// for more details.
// @httpMethod: "GET" or "POST".
// @params: A dictionary that contains your request parameters.
// @postDataType: "GET" for kWBRequestPostDataTypeNone, "POST" for kWBRequestPostDataTypeNormal or kWBRequestPostDataTypeMultipart.
// @httpHeaderFields: A dictionary that contains HTTP header information.
- (void)loadRequestWithUrl:(NSString *)url
                httpMethod:(NSString *)httpMethod
                    params:(NSDictionary *)params
              postDataType:(OFRequestPostDataType)postDataType
          httpHeaderFields:(NSDictionary *)httpHeaderFields;




//使用APIKey的请求
- (void)loadApiKeyRequestWithUrl:(NSString *)url httpMethod:(NSString *)httpMethod params:(NSDictionary *)params postDataType:(OFRequestPostDataType)postDataType httpHeaderFields:(NSDictionary *)httpHeaderFields;
- (void)cancelRequest;

#pragma mark - API
/*
 * statuses/repost 转发一条微博信息
 */
- (void)repostWeiboWithWeiboId:(NSString *)weiboId
                          text:(NSString *)text
                   commentType:(OFRepostCommentType)commentType;

/*
 * statuses/update 发布一条微博信息
 * statuses/upload 上传图片并发布一条微博
 */
- (void)sendWeiBoWithText:(NSString *)text image:(UIImage *)image;

/*
 * comments/create 评论一条微博
 */
//
- (void)sendCommentWithWeiboId:(NSString *)weiboId
                          text:(NSString *)text
                    commentOri:(BOOL)commentOri;
@end

@interface OFEngine (Private)
- (void)startAuthorize;
- (void)saveAuthorizeData;
- (void)readAuthorizeData;
- (void)deleteAuthorizeData;
- (NSString *)urlSchemeString;
- (id)parseJSONString:(NSString *)string error:(NSError **)error;
- (OFRequest *)generateRequestWithUrl:(NSString *)url
                           httpMethod:(NSString *)httpMethod
                               params:(NSMutableDictionary *)params
                         postDataType:(OFRequestPostDataType)postDataType
                     httpHeaderFields:(NSDictionary *)httpHeaderFields
                             delegate:(id<OFRequestDelegate>)delegate;

- (void)delegateEngineAlreadyLoggedIn;
- (void)delegateEngineDidLogIn;
- (void)delegateEngineDidFailToLogInWithError:(NSError *)error;
- (void)delegateEngineDidLogOut;
- (void)delegateEngineNotAuthorized;
- (void)delegateEngineAuthorizeExpired;
- (void)delegateEngineRequestDidFailWithError:(NSError *)error;
- (void)delegateEngineRequestDidSucceedWithResult:(id)result;

@end
