//
//  OFAuthorize.m
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFAuthorize.h"
#import "OFSDKGlobal.h"

@interface OFAuthorize ()
- (void)delegateAuthorizeDidSucceed;
- (void)delegateAuthorizeDidFailWithError:(NSError *)error;
@end

@implementation OFAuthorize
@synthesize request = _request;
@synthesize delegate = _delegate;
@synthesize webViewController = _webViewController;

#pragma mark - WBAuthorize Life Circle
- (void)dealloc
{
    [self resetOFRequest];
    [self resetWebView];
    [_delegate release], _delegate = nil;
    
    [super dealloc];
}

#pragma mark - WBAuthorize Public Methods
- (void)startAuthorize
{
    OFAuthorizeWebView *controller = [[OFAuthorizeWebView alloc] init];
    [controller setDelegate:self];
    self.webViewController = controller;
    [controller release];
}

- (void)handleAuthorizeCancel
{
    [self retain];
    [self resetOFRequest];
    [self resetWebView];
    [self delegateAuthorizeDidFailWithError:nil];
    [self release];
}

- (void)handleAuthorizeFailWithError:(NSError *)error
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil
                                                     message:@"授权失败"
                                                    delegate:self
                                           cancelButtonTitle:@"确定"
                                           otherButtonTitles:nil];
    [alert show];
    [alert release], alert = nil;
}

- (void)handleAuthorizeSuccess
{
    [self retain];
    [self resetOFRequest];
    [self resetWebView];
    [self delegateAuthorizeDidSucceed];
    [self release];
}

- (void)resetWebView
{
    if (self.webViewController.navigationController) {
        if ([self.webViewController.navigationController.viewControllers objectAtIndex:0] == self.webViewController) {
            [self.webViewController dismissModalViewControllerAnimated:YES];
        }else{
            [self.webViewController.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [self.webViewController dismissModalViewControllerAnimated:YES];
    }
    
    self.webViewController.delegate = nil;
    [_webViewController release] ,_webViewController = nil;
}

- (void)resetOFRequest
{
    [self.request setDelegate:nil];
    [self.request disconnect];
    self.request = nil;
}

#pragma mark - OFAuthorizeWebViewDelegate
- (void)authorizeWebView:(OFAuthorizeWebView *)webView startLoadWithParamsString:(NSString *)paramsString
{
    return;
}

- (void)authorizeWebViewDidCancel:(OFAuthorizeWebView *)webView
{
    if (webView == self.webViewController) {
        [self handleAuthorizeCancel];
    }
}

#pragma mark - OFRequestDelegate Methods
- (void)request:(OFRequest *)theRequest didFinishLoadingWithResult:(id)result
{
    return;
}

- (void)request:(OFRequest *)theReqest didFailWithError:(NSError *)error
{
    [self retain];
    [self handleAuthorizeFailWithError:error];
    [self release];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self retain];
    [self resetOFRequest];
    [self resetWebView];
    [self delegateAuthorizeDidFailWithError:nil];
    [self release];
}

#pragma mark - Private
- (void)delegateAuthorizeDidSucceed
{
    if ([_delegate respondsToSelector:@selector(authorizeDidSuccee:)]) {
        [_delegate authorizeDidSuccee:self];
    }
}

- (void)delegateAuthorizeDidFailWithError:(NSError *)error
{
    if ([_delegate respondsToSelector:@selector(authorize:didFailWithError:)]) {
        [_delegate authorize:self didFailWithError:error];
    }
}


@end
