//
//  OFAuthorizeOauth2.m
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFAuthorizeOauth2.h"

@interface OFAuthorizeOauth2 ()
- (NSString  *)dataSourceAuthorizeOauth2AuthorizeURL;
- (OFRequest *)dataSourceAuthorizeOauth2WBRequestForAccessTokenWithAuthorizeCode:(NSString *)code;

- (OFAuthorizeParseResult *)dataDelegateAuthorizeOauth2ParseAuthorizeCode:(NSString *)string;
- (BOOL)dataDelegateAuthorizeOauth2ParseAccessToken:(NSString *)string;
@end

@implementation OFAuthorizeOauth2
@synthesize dataSource = _dataSource;
@synthesize dataDelegate = _dataDelegate;

- (void)dealloc
{
    [_dataDelegate release], _dataDelegate = nil;
    [_dataSource release], _dataSource = nil;
    [super dealloc];
}

/*
 You can get top of your view controllers, then present a new modal from that top view controller.
 You call this method with rootViewController is window's rootViewController
 */
- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (void)startAuthorize
{
    [super startAuthorize];
    [self.webViewController loadRequestWithURL:[NSURL URLWithString:[self dataSourceAuthorizeOauth2AuthorizeURL]]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        self.webViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
//    if ([[DataEnvironment sharedDataEnvironment] weiboViewController]) {
//        [[[[DataEnvironment sharedDataEnvironment] weiboViewController] navigationController] pushViewController:self.webViewController animated:YES];
//    }
    
    UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    UIViewController  *controller = [self topViewController:window.rootViewController];
    if ([controller isKindOfClass:[MKNavigationController class]]) {
        [(MKNavigationController *)controller pushViewController:self.webViewController animated:YES];
    }else
    if (controller.view.bounds.size.width<650 && controller.view.bounds.size.height<700) {
        [controller.navigationController pushViewController:self.webViewController animated:YES];
    }else{
        MKNavigationController *nav = [[MKNavigationController alloc] initWithRootViewController:self.webViewController];
        nav.navigationBarHidden  = YES;
        nav.modalPresentationStyle = UIModalPresentationFormSheet;
        [controller presentModalViewController:nav animated:YES];
        [nav release];
    }
}

#pragma mark - OFAuthorizeWebViewDelegate
- (void)authorizeWebView:(OFAuthorizeWebView *)webView startLoadWithParamsString:(NSString *)paramsString
{
    [self retain];
    OFAuthorizeParseResult * params = [_dataDelegate authorizeOauth2:self parseAuthorizeCode:paramsString];
    if (params && params.authorizeCode) {
        if (!params.authorizeErrorCode) {
            self.request = [self dataSourceAuthorizeOauth2WBRequestForAccessTokenWithAuthorizeCode:params.authorizeCode];
            [self.request connect];
        }
    }
    [self release];
}

#pragma mark - OFRequestDelegate Methods
- (void)request:(OFRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    [self retain];
    NSString * response = result;
    if ([self dataDelegateAuthorizeOauth2ParseAccessToken: response]) {
        [self handleAuthorizeSuccess];
    } else {
        [self handleAuthorizeFailWithError:nil];
    }
    
    [self release];
}

#pragma mark - Private
- (NSString  *)dataSourceAuthorizeOauth2AuthorizeURL
{
    if ([_dataSource respondsToSelector:@selector(authorizeOauth2AuthorizeURL:)]) {
        return [_dataSource authorizeOauth2AuthorizeURL:self];
    }
    return nil;
}

- (OFRequest *)dataSourceAuthorizeOauth2WBRequestForAccessTokenWithAuthorizeCode:(NSString *)code
{
    if ([_dataSource respondsToSelector:@selector(authorizeOauth2WBRequestForAccessToken:authorizeCode:)]) {
        return [_dataSource authorizeOauth2WBRequestForAccessToken:self authorizeCode:code];
    }
    return nil;
}

- (OFAuthorizeParseResult *)dataDelegateAuthorizeOauth2ParseAuthorizeCode:(NSString *)string
{
    if ([_dataDelegate respondsToSelector:@selector(authorizeOauth2:parseAuthorizeCode:)]) {
        return [_dataDelegate authorizeOauth2:self parseAuthorizeCode:string];
    }
    return nil;
}

- (BOOL)dataDelegateAuthorizeOauth2ParseAccessToken:(NSString *)string
{
    if ([_dataDelegate respondsToSelector:@selector(authorizeOauth2:parseAccessToken:)]) {
        return [_dataDelegate authorizeOauth2:self parseAccessToken:string];
    }
    return NO;
}


@end
