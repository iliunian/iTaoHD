//
//  OFAuthorizeWebView.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kDefaultAuthorizeViewWidthForIpad   540
#define kDefaultAuthorizeViewHeightForIpad  620

@class OFAuthorizeWebView;

@protocol OFAuthorizeWebViewDelegate <NSObject>

- (void)authorizeWebView:(OFAuthorizeWebView *)webView startLoadWithParamsString:(NSString *)paramsString;
- (void)authorizeWebViewDidCancel:(OFAuthorizeWebView *)webView;
@end

@interface OFAuthorizeWebView : UIViewController <UIWebViewDelegate>

@property (nonatomic, retain) id<OFAuthorizeWebViewDelegate> delegate;

- (void)loadRequestWithURL:(NSURL *)url;
- (void)startAnimatingIndeicatorView;

@end
