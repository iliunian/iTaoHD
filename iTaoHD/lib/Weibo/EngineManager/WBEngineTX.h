//
//  WBEngineTX.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFEngineOauth2.h"

@interface WBEngineTX :OFEngineOauth2{
    NSString   *_userId;
    NSString   *_openId;
    
}

@property (nonatomic,retain) NSString  *userId;
@property (nonatomic,retain) NSString  *openId;


- (id)initWithAppKey:(NSString *)theAppKey appSecret:(NSString *)theAppSecret;

#pragma mark - API

/*
 *接口 statuses/home_timeline 时间线/主页时间线
 */
- (void)requestFriendsTimelineWithSinceId:(NSString *)sinceId
                                    maxId:(NSString *)maxId
                                   length:(NSInteger)length;

/*
 *接口：t/re_add 微博相关/转播一条微博
 */
- (void)repostWeiboWithWeiboId:(NSString *)weiboId
                          text:(NSString *)text
                   commentType:(OFRepostCommentType)commentType;

/*
 *接口: t/re_list 微博相关/获取单条微博的转发或点评列表
 */
- (void)requestCommentsWithWeiboId:(NSString *)weiboId
                             maxId:(NSString *)maxId
                      maxTimestamp:(NSString *)maxTimestamp
                            length:(NSInteger)length;

/*
 * 接口：t/re_count 微博相关/转播数或点评数
 */
- (void)requestCountsWithWeiboIds:(NSArray *)weiboIds;

/*
 * 接口：user/info  获取自己的详细资料
 */
- (void)requestUserProfile;

/*
 * 帐户相关/获取其他人资料
 */
- (void)requestOtherUserProfile:(NSString *)uId;
@end
