//
//  WBEngineSina.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFEngineOauth2.h"

@interface WBEngineSina :OFEngineOauth2{
    NSString * _userId;
    BOOL _ssoLoggingIn;
}

@property (nonatomic,retain) NSString * userId;

- (id)initWithAppKey:(NSString *)theAppKey appSecret:(NSString *)theAppSecret;

- (BOOL)handleOpenURL:(NSURL *)url;

#pragma mark - API

/*
 * statuses/friends_timeline 获取当前登录用户及其所关注用户的最新微博
 */
- (void)requestFriendsTimelineWithSinceId:(NSString *)sinceId
                                    maxId:(NSString *)maxId
                                   length:(NSInteger)length;

/*
 * statuses/upload_url_text 发布一条微博同时指定上传的图片或图片url
 */
//需要高级授权，暂时不实现
//- (void)sendWeiboWithText:(NSString *)text imageUrl:(NSString *)imageUrl;

/*
 * statuses/counts 获取评论和转发数
 */
- (void)requestCountsWithWeiboIds:(NSArray *)weiboIds;

/*
 * comments/show 获取某条微博的评论列表
 */
- (void)requestCommentsWithWeiboId:(NSString *)weiboId
                           sinceId:(NSString *)sinceId
                             maxId:(NSString *)maxId
                            length:(NSInteger)length;


/*
 * users/show 获取用户信息
 */
- (void)requestUserProfile;

/*
 * 帐户相关/获取其他人资料
 */
- (void)requestOtherUserProfile:(NSString *)uId;

#pragma mark 转换短链接 使用AppKey
- (void)covertToShort_urlWithLongUrl:(NSString *)url;
@end
