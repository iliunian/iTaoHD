//
//  QZoneEngine.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFEngineOauth2.h"

@interface QZoneEngine : OFEngineOauth2{
    NSString   *_openId;
}
@property (nonatomic,retain) NSString  *openId;


- (id)initWithAppId:(NSString *)theAppId appKey:(NSString *)theAppKey;

/*
 * 接口：get_user_info  获取自己的详细资料
 */
- (void)requestUserProfile;

/*
 * 帐户相关/获取其他人资料
 */
- (void)requestOtherUserProfile:(NSString *)uId;

/*
 *接口：upload_pic  上传一张照片到QQ空间相册
 *需要申请接口权限
 *HTTP请求方式：POST
 */
- (void)uploadPicToQZone:(UIImage *)image;

/*
 *分享内容到QZone
 */
- (void)shareToQZoneWithTitle:(NSString *)title
                      comment:(NSString *)commentText
                     titleUrl:(NSString *)titleUrl
                      summary:(NSString *)summary
                        image:(UIImage *)image;

/*
 *接口：add_share
 *HTTP请求方式：POST
 *参数名称	是否必须	 类型	       描述
 *title	    必须	    string	   feeds的标题
 *url	    必须	    string	   分享所在网页资源的链接
 *comment		    string	   用户评论内容，也叫发表分享时的分享理由
 *summary		    string	   所分享的网页资源的摘要内容，或者是网页的概要描述
 *imageUrl		    string	   所分享的网页资源的代表性图片链接"
 *
 *参数详情见：http://wiki.opensns.qq.com/wiki/【QQ登录】add_share
 */
- (void)shareToQZoneWithTitle:(NSString *)title
                      comment:(NSString *)commentText
                     titleUrl:(NSString *)titleUrl
                      summary:(NSString *)summary
                     imageUrl:(NSString *)imageUrl;
@end
