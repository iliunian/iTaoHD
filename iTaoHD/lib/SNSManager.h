//
//  SNSManager.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
#import "OFEngine.h"


typedef enum {
    SNSManagerErrorTypeNetwork,
    SNSManagerErrorTypeServer,
    SNSManagerErrorTypeNotAuthorized
} SNSManagerErrorTypeE;

typedef enum {
    SNSManagerRequestTypeLogin,                  //Login
    SNSManagerRequestTypeLogout,                 //logout
    SNSManagerRequestTypeSendContent,           //share content
    SNSManagerRequestTypeUserProfile,            //user info
    SNSManagerRequestTypeShortenUrl,            //shorten url
} SNSManagerRequestTypeE;

@protocol SNSManagerDelegate <NSObject>
@optional
//login or logout
- (void)snsManagerLoginDidSuccessFor:(OFTypeE)type;
- (void)snsManagerLoginDidFailFor:(OFTypeE)type;
- (void)snsManagerLogoutDidFinishFor:(OFTypeE)type;

//token Expired callback
- (void)snsManagerAuthorizeExpired;

//UserProfile callback
- (void)snsManagerUserProfileDidFailFor:(OFTypeE)type;
- (void)snsManagerUserProfileDidSuccessFor:(OFTypeE)type withResult:(id)result;

//Shorten Url
- (void)snsManagerShortenUrlDidFailFor:(OFTypeE)type;
- (void)snsManagerShortenUrlDidSuccessFor:(OFTypeE)type withResult:(id)result;

//share content
- (void)snsManagerShareContentDidSuccessFor:(OFTypeE)type weiboId:(NSString *)weiboId;
- (void)snsManagerShareContentDidFailFor:(OFTypeE)type withError:(SNSManagerErrorTypeE)error;

@end

@interface SNSManager : NSObject<OFEngineDelegate>{
    NSMutableArray    *_requestsArray;
}

+ (SNSManager *)sharedManager;

- (OFEngine *)getOFEngineByOFType:(OFTypeE)type;
- (void)cancelRequestForDelegate:(id<SNSManagerDelegate>)delegate;

- (BOOL)handleOpenCallbackURL:(NSURL *)url;

#pragma mark - login or logout
- (void)loginFor:(OFTypeE)type withDelegate:(id<SNSManagerDelegate>)delegate;
- (void)logoutFor:(OFTypeE)type withDelegate:(id<SNSManagerDelegate>)delegate;
- (BOOL)isLoggedInFor:(OFTypeE)type;

/*
 * users/show 获取用户信息
 */
- (void)requestUserProfile:(id<SNSManagerDelegate>)delegate type:(OFTypeE)type;

//send content for sina or tx
- (void)sendContentWithText:(NSString *)text
                      image:(UIImage *)image
                   delegate:(id<SNSManagerDelegate>)delegate
                    forType:(OFTypeE)type;

/*
 *分享到QQ空间，带imgage
 *注：title，titleUrl为必填项
 */
- (void)shareToQZoneWithTitle:(NSString *)title
                      comment:(NSString *)commentText
                     titleUrl:(NSString *)titleUrl
                      summary:(NSString *)summary
                        image:(UIImage *)image
                     delegate:(id<SNSManagerDelegate>)delegate
                      forType:(OFTypeE)type;

/*
 *分享到QQ空间，带imageUrl
 *注：title，titleUrl为必填项
 */
- (void)shareToQZoneWithTitle:(NSString *)title
                      comment:(NSString *)commentText
                     titleUrl:(NSString *)titleUrl
                      summary:(NSString *)summary
                     imageUrl:(NSString *)imageUrl
                     delegate:(id<SNSManagerDelegate>)delegate
                      forType:(OFTypeE)type;

- (void)covertToShotURL:(NSString *)longURL withDelegate:(id<SNSManagerDelegate>)delegate;

@end
