//
//  BMTipsView.h
//  ZJNews
//
//  Created by Tang Haibo on 11-11-11.
//  Modify by meng lv on 12-6-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum _PopDir
{
    PopDirTop = 0,
    PopDirBottom,
}PopDir;

@interface BMTipsView : UIView
{
    PopDir _toDir;
    NSString *_tips;
    UIView *_inview;
    
    BOOL _autoDismiss;
    NSInteger _dismisSec;   //多少秒自动关闭
    CGSize _size;
    
    UILabel *_titleLabel;
}

@property (nonatomic, copy) NSString *tips;
@property (nonatomic, assign) BOOL  isNightStyle;
@property (nonatomic, retain) UIView *inview;

#pragma mark - functions
- (void)dismiss;

- (void)showTips;

/*
 *  功能：   根据参数从fromDir方向弹出提示，可以选择是否自动消失  
 *  输入参数：tips            提示内容
 *          toDir           向该方向弹出提示
 *          size:           显示的size
 *          view            要将该视图在view中弹出提示
 *          autoDismiss     是否自动消失
 *          sec             在sec秒内消失。该参数只在autoDismiss为YES时，才有效
 *  返回值：   
 *  修改记录：
 */
#pragma mark init
- (id)initWithTips:(NSString *)tips 
           andSize:(CGSize)size
             toDir:(PopDir)toDir
            inView:(UIView*)view 
       autoDismiss:(BOOL)autoDismiss 
             inSec:(NSInteger)sec
         textColor:(UIColor *)textColor
         backColor:(UIColor *)backColor;
@end
