//
//  UIImage+UIImage_Gray.h
//  BMTV
//
//  Created by Tang Haibo on 11-11-8.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Gray)
- (UIImage *)grayImage;
@end
