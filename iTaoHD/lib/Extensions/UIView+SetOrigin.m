//
//  UIView+SetOrigin.m
//  LimitFreeMaster
//
//  Created by Haibo Tang on 11-8-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "UIView+SetOrigin.h"


@implementation UIView (UIView_SetOrigin)
- (void)setOrigin:(CGPoint)origin{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGFloat)setOriginY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
    return CGRectGetMaxY(frame);
}

- (void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}
- (void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}
- (void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}
//getter
- (float)rightx{
    CGRect frame = self.frame;
    return frame.origin.x+frame.size.width;
}
- (float)bottomy{
    CGRect frame = self.frame;
    return frame.origin.y+frame.size.height;
}

- (float)width{
    CGRect frame = self.frame;
    return frame.size.width;
}

- (float)height{
    CGRect frame = self.frame;
    return frame.size.height;
}

- (CGSize)size{
    return self.frame.size;
}

- (void)safeSetCenter:(CGPoint)center{
    [self setCenter:center];
    CGRect frame = self.frame;
    frame.origin = CGPointMake(roundf(frame.origin.x), roundf(frame.origin.y));
    self.frame = frame;
}
@end
