//
//  UIView+Nib.h
//  Installation
//
//  Created by Haibo Tang on 12-9-21.
//  Copyright (c) 2012年 Banma.com. All rights reserved.
//



@interface UIView (Nib)

- (id)initFromNib;
@end
